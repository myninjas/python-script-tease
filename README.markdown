# Python Script Tease

![](https://img.shields.io/badge/status-active-green.svg)
![](https://img.shields.io/badge/stage-development-blue.svg)
![](https://img.shields.io/badge/coverage-0%25-red.svg)

A collection of classes and commands for automated command line scripting using Python.