import os
from script_tease.commands.templates import *
from script_tease.factory import factory


# Tests


def test_itemized_simple_template():
    args = (
        os.path.join("tests", "templates", "$item"),
        os.path.join("tests", "output", "$item"),
    )
    kwargs = {
        'context': {
            'deploy_user': "deploy",
            'domain_name': "example.com",
            'domain_tld': "example_com",
        },
        'items': (
            "example1.cfg",
            "example2.cfg",
        ),
        'parser': Template.PARSER_SIMPLE,
    }
    comment = "test simple template with itemized command"
    t = factory("template", comment, *args, **kwargs)
    for c in t.get_commands():
        content = c.get_content()
        assert "deploy" in content
        assert "example.com" in content
        assert "example_com" in content

    # [process vault templates]
    # template = {{ ITEM_PATH }}/skeleton/deploy/vault/$item ./deploy/vault/$item
    # items = live.env.ini, staging.env.ini
    # backup = no


class TestTemplateJinja:

    def test_get_content(self):
        pass

    def test_get_statement(self):
        pass

    def test_get_template(self):
        pass

    def test_preview(self):
        pass

    def test_get_command(self):
        pass

    def test_get_jinja2_statement(self):
        pass

    # def test_get_simple_statement(self):
    #     context = {
    #         'deploy_user': "deploy",
    #         'domain_name': "example.com",
    #         'domain_tld': "example_com",
    #     }
    #     source = os.path.join("tests", "templates", "example.cfg")
    #     target = os.path.join("tests", "output", "example.cfg")
    #     t = Template(source, target, context=context, parser=Template.PARSER_SIMPLE)
    #
    #     # domain_name = $domain_name$
    #     # domain_tld = $domain_tld$
    #     # deploy_user = $deploy_user$


class TestTemplateSimple:

    def test_get_content(self):
        context = {
            'deploy_user': "deploy",
            'domain_name': "example.com",
            'domain_tld': "example_com",
        }
        source = os.path.join("tests", "templates", "example1.cfg")
        target = os.path.join("tests", "output", "example.cfg")
        t = Template(source, target, context=context, parser=Template.PARSER_SIMPLE)
        content = t.get_content()
        assert "deploy" in content
        assert "example.com" in content
        assert "example_com" in content

    def test_get_statement(self):
        pass

    def test_get_template(self):
        pass

    def test_preview(self):
        pass

    def test_get_command(self):
        pass

    # def test_get_simple_statement(self):
    #     context = {
    #         'deploy_user': "deploy",
    #         'domain_name': "example.com",
    #         'domain_tld': "example_com",
    #     }
    #     source = os.path.join("tests", "templates", "example.cfg")
    #     target = os.path.join("tests", "output", "example.cfg")
    #     t = Template(source, target, context=context, parser=Template.PARSER_SIMPLE)
    #
    #     # domain_name = $domain_name$
    #     # domain_tld = $domain_tld$
    #     # deploy_user = $deploy_user$
