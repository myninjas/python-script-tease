"""
SSL
...

Classes for generating SSL certificates.

"""
# Imports

from deployable.commands import Command
from ..base import Step

# Exports

__all__ = (
    "MAPPING",
    "Certbot",
)

# Classes


class Certbot(Step):
    """Generate an SSL certificate using Let's Encrypt."""

    def __init__(self, domain_name, email=None, webroot=None, **kwargs):
        self.domain_name = domain_name
        self.email = email
        self.webroot = webroot

        name = kwargs.get("comment", "create SSL cert for %s" % domain_name)
        super().__init__(name, **kwargs)

    def get_commands(self):
        context = {
            'domain_name': self.domain_name,
            'email': self.email,
            'webroot': self.webroot,
        }
        template = "certbot certonly --agree-tos --email %(email)s -n --webroot -w %(webroot)s -d %(domain_name)s"

        statement = template % context

        return [Command(statement, **self._command_kwargs)]


MAPPING = {
    'certbot': Certbot,
}
