"""
Users
.....

Classes for working with user accounts.

"""
# Imports

from deployable.commands import Command
from ..base import Step

# Exports

__all__ = (
    "MAPPING",
    "AddUser",
)

# Classes


class AddUser(Step):
    """Add a user account."""

    def __init__(self, name, groups=None, home=None, **kwargs):
        self.groups = groups
        self.home = home
        self.user_name = name

        _name = kwargs.get("comment", "create user %s" % name)
        super().__init__(_name, **kwargs)

    def get_commands(self):
        commands = list()

        # The gecos switch eliminates the prompts.
        statement = ['adduser %s --disable-password --gecos ""' % self.user_name]

        if self.home is not None:
            statement.append("--home %s" % self.home)

        commands.append(Command(statement, **self._command_kwargs))

        if self.groups is not None:
            for group in self.groups:
                statement = "adduser %s %s" % (self.user_name, group)
                commands.append(Command(statement, **self._command_kwargs))

            if "sudo" in self.groups:
                statement = 'echo "%s ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/%s' % (self.user_name, self.user_name)
                commands.append(Command(statement, **self._command_kwargs))

        return commands


MAPPING = {
    'adduser': AddUser,
}
