# Imports

from deployable.commands import Command
from ..base import Step

# Exports

__all__ = (
    "MAPPING",
    "Run",
)

# Classes


class Run(Step):
    """Execute a command."""

    def __init__(self, statement, **kwargs):
        self.statement = statement

        name = kwargs.get("comment", "run a command")
        super().__init__(name, **kwargs)

    def get_commands(self):
        return [Command(self.statement, **self._command_kwargs)]


MAPPING = {
    'run': Run,
}
