"""
Services
........

Classes for working with commands related to starting and stopping server services.

"""
# Imports

from deployable.commands import Command
from ..base import Step

# Exports

__all__ = (
    "MAPPING",
    "Reload",
    "Restart",
    "Start",
    "Stop",
)

# Classes


class Reload(Step):
    """Reload a service."""

    def __init__(self, service, **kwargs):
        self.service = service

        name = kwargs.get("comment", "reload the %s service" % service)
        super().__init__(name, **kwargs)

    def get_commands(self):
        return [Command("service %s reload" % self.service, **self._command_kwargs)]


class Restart(Step):
    """Restart a service."""

    def __init__(self, service, **kwargs):
        self.service = service

        name = kwargs.get("comment", "restart the %s service" % service)
        super().__init__(name, **kwargs)

    def get_commands(self):
        return [Command("service %s restart" % self.service, **self._command_kwargs)]


class Start(Step):
    """Start a service."""

    def __init__(self, service, **kwargs):
        """Stop the named service.

        :param service: The name of the service.
        :type service: str

        """
        self.service = service

        name = kwargs.get("comment", "start the %s service" % service)
        super().__init__(name, **kwargs)

    def get_commands(self):
        return [Command("service %s start" % self.service, **self._command_kwargs)]


class Stop(Step):
    """Stop a service."""

    def __init__(self, service, **kwargs):
        """Stop the named service.

        :param service: The name of the service.
        :type service: str

        """
        self.service = service

        name = kwargs.get("comment", "stop the %s service" % service)
        super().__init__(name, **kwargs)

    def get_commands(self):
        return [Command("service %s stop" % self.service, **self._command_kwargs)]


MAPPING = {
    'reload': Reload,
    'restart': Restart,
    'start': Start,
    'stop': Stop,
}
