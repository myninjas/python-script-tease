"""
Django
......

Classes for working with commands related to Django management commands.

"""
# Imports

from deployable.commands import Command
from deployable.constants import ENVIRONMENT_BASE, SCOPE_DEPLOY
from ..base import Step

# Exports

__all__ = (
    "MAPPING",
    "Django",
)

# Classes


class Django(Step):
    """Run a Django management command (additional options are interpreted as options for the command)."""

    def __init__(self, management_command, **kwargs):
        self.management_command = management_command

        name = kwargs.get("comment", "run %s django management command" % management_command)

        _kwargs = {
            'comment': kwargs.pop("comment", None),
            'cwd': kwargs.pop("cwd", None),
            'environments': kwargs.pop("environments", [ENVIRONMENT_BASE]),
            'prefix': kwargs.pop("prefix", None),
            'scope': kwargs.pop("scope", SCOPE_DEPLOY),
            'shell': kwargs.pop("shell", "/bin/bash"),
            'stop': kwargs.pop("stop", False),
            'sudo': kwargs.pop("sudo", True),
            'tags': kwargs.pop("tags", list()),
        }

        self.options = dict()
        for key, value in kwargs.items():
            self.options[key] = value

        super().__init__(name, **_kwargs)

    def get_commands(self):
        statement = ["./manage.py %s" % self.management_command]

        for key, value in self.options.items():
            statement.append("--%s=%s" % (key, value))

        return [Command(statement, **self._command_kwargs)]


MAPPING = {
    'django': Django,
}
