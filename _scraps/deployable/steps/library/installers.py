# Imports

# import os
from deployable.commands import Command
from ..base import Step

# Exports

__all__ = (
    "MAPPING",
    "Apt",
    "Pip",
    "VirtualEnv",
    "Yum",
)

# Classes


class Apt(Step):
    """Install a package using apt-get."""

    def __init__(self, package, **kwargs):
        self.package = package

        name = kwargs.get("comment", "install %s" % package)

        super().__init__(name, **kwargs)

    def get_commands(self):
        statement = "apt-get install -y %s" % self.package

        return [Command(statement, **self._command_kwargs)]


# class Install(Step):
#
#     def __init__(self, package, **kwargs):
#         self.package = package
#
#         name = kwargs.get("comment", "install %s" % package)
#
#         super().__init__(name, **kwargs)
#
#     def get_commands(self):
#         pass


class Pip(Step):
    """Install a Python package using pip."""

    def __init__(self, package, **kwargs):
        self.package = package

        name = kwargs.get("comment", "install %s" % package)

        super().__init__(name, **kwargs)

    def get_commands(self):
        statement = "pip3 install %s" % self.package

        return [Command(statement, **self._command_kwargs)]


class VirtualEnv(Step):
    """Create a Python virtual environment."""

    def __init__(self, name, **kwargs):
        self.environment_name = name

        _name = kwargs.get("comment", "create %s virtual environment" % name)

        super().__init__(_name, **kwargs)

    def get_commands(self):
        statement = "virtualenv %s" % self.environment_name

        return [Command(statement, **self._command_kwargs)]


class Yum(Step):
    """Install a package using yum."""

    # http://man7.org/linux/man-pages/man8/yum.8.html

    def __init__(self, package, **kwargs):
        self.package = package

        name = kwargs.get("comment", "install %s" % package)

        super().__init__(name, **kwargs)

    def get_commands(self):
        statement = "yum -y install %s" % self.package

        return [Command(statement, **self._command_kwargs)]


MAPPING = {
    'apt': Apt,
    'pip': Pip,
    'virtualenv': VirtualEnv,
    'yum': Yum,
}
