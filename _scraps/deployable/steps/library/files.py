# Imports

import os
from deployable.commands import Command
from ..base import Step

# Exports

__all__ = (
    "MAPPING",
    "Append",
    "Copy",
    "MakeDir",
    "Permissions",
    "Remove",
    "Sed",
    "Symlink",
    "Touch",
)

# Classes


class Append(Step):
    """Append to a file."""

    def __init__(self, path, content=None, **kwargs):
        """Append content to a file.

        :param path: The path to the file.
        :type path: str

        :param content: The content to be appended.
        :type content: str

        """
        self.content = content or ""
        self.path = path

        name = kwargs.get("comment", "append to %s" % path)

        super().__init__(name, **kwargs)

    def get_commands(self):
        return [
            Command('echo "%s" >> %s' % (self.content, self.path), **self._command_kwargs)
        ]


class Copy(Step):
    """Copy a file or directory."""

    def __init__(self, from_path, to_path, recursive=False, **kwargs):
        self.from_path = from_path
        self.recursive = recursive
        self.to_path = to_path

        name = kwargs.get("comment", "copy %s" % from_path)

        super().__init__(name, **kwargs)

    def get_commands(self):
        statement = ["cp"]

        if self.recursive:
            statement.append("-R")

        statement.append(self.from_path)
        statement.append(self.to_path)

        return [Command(statement, **self._command_kwargs)]


class MakeDir(Step):
    """Create a directory."""

    def __init__(self, path, mode=None, recursive=True, **kwargs):
        self.mode = mode
        self.path = path
        self.recursive = recursive

        name = kwargs.get("comment", "make %s" % path)

        super().__init__(name, **kwargs)

    def get_commands(self):
        statement = ["mkdir"]

        if self.mode:
            statement.append("-m %s" % self.mode)

        if self.recursive:
            statement.append("-p")

        statement.append(self.path)

        return [Command(statement, **self._command_kwargs)]


class Permissions(Step):
    """Set permissions on a file or directory."""

    def __init__(self, path, group=None, mode=None, owner=None, recursive=False, **kwargs):
        self.group = group
        self.mode = mode
        self.owner = owner
        self.path = path
        self.recursive = recursive

        name = kwargs.get("comment", "set permissions on %s" % path)

        super().__init__(name, **kwargs)

    def get_commands(self):
        commands = list()

        if self.group is not None:
            statement = ["chgrp"]

            if self.recursive:
                statement.append("-R")

            statement.append(self.group)
            statement.append(self.path)

            commands.append(Command(statement, **self._command_kwargs))

        if self.owner is not None:
            statement = ["chown"]

            if self.recursive:
                statement.append("-R")

            statement.append(self.owner)
            statement.append(self.path)

            commands.append(Command(statement, **self._command_kwargs))

        if self.mode is not None:
            statement = ["chmod"]

            if self.recursive:
                statement.append("-R")

            statement.append(str(self.mode))
            statement.append(self.path)

            commands.append(Command(statement, **self._command_kwargs))

        return commands


class Remove(Step):
    """Remove a file or directory."""

    def __init__(self, path, force=False, recursive=False, **kwargs):
        self.force = force
        self.path = path
        self.recursive = recursive

        name = kwargs.get("comment", "remove %s" % path)

        super().__init__(name, **kwargs)

    def get_commands(self):
        statement = ["rm"]

        if self.force:
            statement.append("-f")

        if self.recursive:
            statement.append("-r")

        statement.append(self.path)

        return [Command(statement, **self._command_kwargs)]


class SCopy(Step):
    """Copy a file from the local (machine) to the remote host."""

    def __init__(self, from_path, to_path, host=None, key_file=None, port=22, user=None, **kwargs):
        self.from_path = from_path
        self.host = host
        self.key_file = key_file
        self.port = port
        self.to_path = to_path
        self.user = user

        name = kwargs.get("comment", "copy %s to remote" % from_path)

        kwargs['local'] = True
        kwargs['sudo'] = False

        super().__init__(name, **kwargs)

    def get_commands(self):
        statement = ["scp"]

        if self.key_file is not None:
            statement.append("-i %s" % self.key_file)

        statement.append("-P %s" % self.port)
        statement.append(self.from_path)

        if self.host is not None and self.user is not None:
            statement.append("%s@%s:%s" % (self.user, self.host, self.to_path))
        else:
            statement.append(self.to_path)

        return [Command(statement, **self._command_kwargs)]


class Sed(Step):
    """Replace text in a file."""

    def __init__(self, path, backup=".b", delimiter="/", find=None, replace=None, **kwargs):
        self.backup = backup
        self.delimiter = delimiter
        self.find = find
        self.path = path
        self.replace = replace

        name = kwargs.get("comment", "find and replace in %s" % path)

        super().__init__(name, **kwargs)

    def get_commands(self):
        context = {
            'backup': self.backup,
            'delimiter': self.delimiter,
            'path': self.path,
            'pattern': self.find,
            'replace': self.replace,
        }

        template = "sed -i %(backup)s 's%(delimiter)s%(pattern)s%(delimiter)s%(replace)s%(delimiter)sg' %(path)s"

        statement = template % context

        return [Command(statement, **self._command_kwargs)]


class Symlink(Step):
    """Create a symlink."""

    def __init__(self, source, force=False, target=None, **kwargs):
        self.force = force
        self.source = source
        self.target = target or os.path.basename(source)

        name = kwargs.get("comment", "link to %s" % source)

        super().__init__(name, **kwargs)

    def get_commands(self):
        statement = ["ln -s"]

        if self.force:
            statement.append("-f")

        statement.append(self.source)
        statement.append(self.target)

        return [Command(statement, **self._command_kwargs)]


class Touch(Step):
    """Touch a file or directory."""

    def __init__(self, path, **kwargs):
        self.path = path

        name = kwargs.get("comment", "touch %s" % path)

        super().__init__(name, **kwargs)

    def get_commands(self):
        return [
            Command("touch %s" % self.path, **self._command_kwargs)
        ]


MAPPING = {
    'append': Append,
    'copy': Copy,
    'cp': Copy,
    'link': Symlink,
    'makedir': MakeDir,
    'mkdir': MakeDir,
    'perm': Permissions,
    'permissions': Permissions,
    'perms': Permissions,
    'remove': Remove,
    'replace': Sed,
    'rm': Remove,
    'scp': SCopy,
    'sed': Sed,
    'symlink': Symlink,
    'touch': Touch,
}
