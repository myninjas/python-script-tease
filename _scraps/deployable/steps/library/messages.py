"""
Messages
........

Classes for creating feedback and sending messages.

"""
# Imports

from deployable.commands import Command
from ..base import Step

# Exports

__all__ = (
    "MAPPING",
    "Message",
    # "Slack",
)

# Classes


class Message(Step):
    """Echo a message to the console."""

    def __init__(self, output, **kwargs):
        self.output = output

        name = kwargs.get("comment", "send a message")
        super().__init__(name, **kwargs)

    def get_commands(self):
        return [Command('echo "%s"' % self.output, **self._command_kwargs)]


# class Slack(Step):
#     """(NOT IMPLEMENTED) Post a message to Slack."""
#
#     def __init__(self, message, token, **kwargs):
#         self.color = kwargs.pop("color", "good")
#         self.message = message
#         self.token = token
#         self.url = "https://slack.com/url/to/be/determined"
#
#         # TODO: Implement the Slack command.
#
#     def preview(self):
#         """There's nothing to see here."""
#         return self.url


MAPPING = {
    'echo': Message,
    'feedback': Message,
    'message': Message,
    'msg': Message,
    # 'slack': Slack,
}
