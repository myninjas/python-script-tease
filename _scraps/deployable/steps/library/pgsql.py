"""
PGSQL
.....

Command classes for working with PostgreSQL.

.. important::
    The ``admin_pass`` argument that is common to the commands in this library defaults to ``None`` when the commands
    are instantiated. However, if the command is generated from a parsed template file using
    :py:class:`myninjas.shell.scripts.Script`, the absence of a value in the context will be rendered as "None" (a
    ``str``) rather than an empty value.

    This will result in a command that looks like:

    ``export PGPASSWORD="None" ...``

    This will, of course, fail when executed. To avoid this, make sure you deal with possible ``None`` type variables
    for ``admin_pass``. For example, ``admin_pass {{ db_admin_pass or "" }}`` in your template.

"""
# Imports

from deployable.commands import Command
from ..base import Step

# Exports

__all__ = (
    "MAPPING",
    "CreateDatabase",
    "CreateUser",
    "DatabaseExists",
    "DropDatabase",
    "DropUser",
    "DumpDatabase",
    "BasePGStep",
    "PSQL",
)

# Classes


class BasePGStep(Step):
    """Base class for Postgres commands."""

    # noinspection PyUnresolvedReferences
    def _get_base_command(self, command_name, password=None):
        tokens = list()

        if password is not None:
            tokens.append('export PGPASSWORD="%s" &&' % password)

        tokens.append("%s -h %s -U %s" % (
            command_name,
            self.db_host,
            self.admin_user,
        ))

        return tokens


class CreateDatabase(BasePGStep):
    """Create a Postgres database."""

    def __init__(self, name, admin_pass=None, admin_user="postgres", host="localhost", owner=None, **kwargs):
        self.admin_pass = admin_pass
        self.admin_user = admin_user
        self.db_host = host
        self.db_name = name
        self.db_owner = owner

        # Postgres commands always run without sudo because the -U may be provided.
        kwargs['sudo'] = False

        _name = kwargs.get("comment", "create the %s database" % name)
        super().__init__(_name, **kwargs)

    def get_commands(self):
        statement = self._get_base_command("createdb", password=self.admin_pass)

        if self.db_owner is not None:
            statement.append("-O %s" % self.db_owner)

        statement.append(self.db_name)

        return [Command(statement, **self._command_kwargs)]


class CreateUser(BasePGStep):
    """Create a Postgres user (role)."""

    def __init__(self, name, admin_pass=None, admin_user="postgres", host="localhost", password=None, **kwargs):
        self.admin_pass = admin_pass
        self.admin_user = admin_user
        self.db_host = host
        self.password = password
        self.user_name = name

        # Postgres commands always run without sudo because the -U may be provided.
        kwargs['sudo'] = False

        _name = kwargs.get("comment", "create the %s user" % name)
        super().__init__(_name, **kwargs)

    def get_commands(self):
        commands = list()

        statement = self._get_base_command("createuser", password=self.admin_pass)
        statement.append("-DRS %s" % self.user_name)

        commands.append(Command(statement, **self._command_kwargs))

        if self.password is not None:
            statement = self._get_base_command("psql")
            statement.append("-c \"ALTER USER %s WITH ENCRYPTED PASSWORD '%s';\"" % (self.user_name, self.password))

            commands.append(Command(statement, **self._command_kwargs))

        return commands


class DatabaseExists(BasePGStep):
    """Determine if a Postgres database exists."""

    def __init__(self, name, admin_pass=None, admin_user="postgres", host="localhost", **kwargs):
        self.admin_pass = admin_pass
        self.admin_user = admin_user
        self.db_host = host
        self.db_name = name

        # Postgres commands always run without sudo because the -U may be provided. However, sudo may be required for
        # file writing.
        # kwargs['sudo'] = False

        _name = kwargs.get("comment", "determine the %s database exists" % name)
        super().__init__(_name, **kwargs)

    def get_commands(self):
        statement = self._get_base_command("psql", password=self.admin_pass)
        statement.append(r"-lqt | cut -d \| -f 1 | grep -qw %s" % self.db_name)

        return [Command(statement, **self._command_kwargs)]


class DropDatabase(BasePGStep):
    """Drop (remove) a Postgres database."""

    def __init__(self, name, admin_pass=None, admin_user="postgres", host="localhost", **kwargs):
        self.admin_pass = admin_pass
        self.admin_user = admin_user
        self.db_host = host
        self.db_name = name

        # Postgres commands always run without sudo because the -U may be provided.
        kwargs['sudo'] = False

        _name = kwargs.get("comment", "remove the %s database" % name)
        super().__init__(_name, **kwargs)

    def get_commands(self):
        statement = self._get_base_command("dropdb", password=self.admin_pass)
        statement.append(self.db_name)

        return [Command(statement, **self._command_kwargs)]


class DropUser(BasePGStep):
    """Drop (remove) a Postgres user (role)."""

    def __init__(self, name, admin_pass=None, admin_user="postgres", host="localhost", **kwargs):
        self.admin_pass = admin_pass
        self.admin_user = admin_user
        self.db_host = host
        self.user_name = name

        # Postgres commands always run without sudo because the -U may be provided.
        kwargs['sudo'] = False

        _name = kwargs.get("comment", "create the %s user" % name)
        super().__init__(_name, **kwargs)

    def get_commands(self):
        statement = self._get_base_command("dropuser", password=self.admin_pass)
        statement.append(self.user_name)

        return [Command(statement, **self._command_kwargs)]


class DumpDatabase(BasePGStep):
    """Dump (export) a Postgres database."""

    def __init__(self, name, admin_pass=None, admin_user="postgres", file_name=None, host="localhost", **kwargs):
        self.admin_pass = admin_pass
        self.admin_user = admin_user
        self.db_host = host
        self.db_name = name
        self.file_name = file_name or kwargs.pop("file_name", "%s.sql" % name)

        # Postgres commands always run without sudo because the -U may be provided.
        kwargs['sudo'] = False

        _name = kwargs.get("comment", "export the %s database" % name)
        super().__init__(_name, **kwargs)

    def get_commands(self):
        statement = self._get_base_command("pg_dump", password=self.admin_pass)
        statement.append("-f %s %s" % (self.file_name, self.db_name))

        return [Command(statement, **self._command_kwargs)]


class PSQL(BasePGStep):
    """Run a Postgres SQL statement."""

    def __init__(self, sql, database="template1", password=None, host="localhost", user="postgres", **kwargs):
        self.db_host = host
        self.db_name = database
        self.db_pass = password
        self.db_user = user
        self.sql = sql

        # Postgres commands always run without sudo because the -U may be provided.
        kwargs['sudo'] = False

        name = kwargs.get("comment", "run an SQL command" )
        super().__init__(name, **kwargs)

    def get_commands(self):
        statement = list()

        if self.db_pass is not None:
            statement.append('export PGPASSWORD="%s" &&' % self.db_pass)

        statement.append("psql -h %s -U %s" % (self.db_host, self.db_user))
        statement.append("-d %s" % self.db_name)
        statement.append('-c "%s"' % self.sql)

        return [Command(statement, **self._command_kwargs)]



MAPPING = {
    'pg.client': PSQL,
    'pg.createdatabase': CreateDatabase,
    'pg.createdb': CreateDatabase,
    'pg.createuser': CreateUser,
    'pg.database': CreateDatabase,
    'pg.databaseexists': DatabaseExists,
    'pg.db': CreateDatabase,
    'pg.dbexists': DatabaseExists,
    'pg.dropdatabase': DropDatabase,
    'pg.dropdb': DropDatabase,
    'pg.dropuser': DropUser,
    'pg.dump': DumpDatabase,
    'pg.dumpdb': DumpDatabase,
    'pg.export': DumpDatabase,
    'pg.user': CreateUser,
    'psql': PSQL,
}
