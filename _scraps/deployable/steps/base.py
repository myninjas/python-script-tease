# Imports

import logging
from deployable.constants import ENVIRONMENT_BASE, SCOPE_DEPLOY

logger = logging.getLogger("deployable")

# Classes


class Step(object):
    """A step includes one or more commands to be executed."""

    def __init__(self, name, *args, **kwargs):
        self.environments = kwargs.pop("environments", [ENVIRONMENT_BASE])
        self.errors = list()
        self.name = name
        self.outputs = list()
        self.results = list()
        self.scope = kwargs.pop("scope", SCOPE_DEPLOY)
        self.tags = kwargs.pop("tags", list())
        self._commands = list()

        self._command_args = args
        self._command_kwargs = kwargs

    def __repr__(self):
        return "<%s %s>" % (self.__class__.__name__, self.name)

    def get_commands(self):
        """Get the commands to be executed for the step.

        :rtype: list[BaseType[Command]]

        """
        return self._commands

    def get_count(self):
        """Get the number of commands to be executed.

        :rtype: int

        """
        return len(self.get_commands())

    def preview(self):
        a = list()
        for c in self.get_commands():
            a.append(c.preview() + ";")

        return "\n".join(a)

    # TODO: Determine whether we will use the Step.run() method.
    # Currently, step commands are executed individually by calling get_commands() in the subcommands.py module.
    #
    # def run(self):
    #
    #     commands = self.get_commands()
    #     for c in commands:
    #         self.results.append(c.run())
    #         self.errors.append(c.error)
    #         self.outputs.append(c.output)
    #
    #     if all(self.results):
    #         return True
    #
    #     return False


class ItemizedStep(Step):
    """A step that repeats the same command for different target items."""

    def __init__(self, cls, items, name, *args, **kwargs):
        self.items = items
        self.step_class = cls

        if "comment" in kwargs:
            name = kwargs['comment']

        super().__init__(name, *args, **kwargs)

    def __repr__(self):
        return "<%s %s>" % (self.__class__.__name__, self.step_class.__name__)

    def get_commands(self):
        """Get the commands to be executed.

        :rtype: list[BaseType[Command]]

        """
        # Return the results of previous calls to the method.
        if len(self._commands) > 0:
            return self._commands

        # Build the commands for item item in the list.
        _kwargs = self._command_kwargs.copy()
        for item in self.items:
            _args = list()
            for a in self._command_args:
                a = a.replace("$item", item)
                _args.append(a)

            step = self.step_class(*_args, **_kwargs)
            self._commands += step.get_commands()

        return self._commands
