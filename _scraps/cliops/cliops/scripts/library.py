# Imports

import logging
from myninjas.utils import write_file
import os
from ..library import Command
from ..constants import LOGGER_NAME

logger = logging.getLogger(LOGGER_NAME)

# Exports

__all__ = (
    "Script",
)

# Classes


class Script(object):
    """A script is a collection of commands."""

    def __init__(self, name, factory=None):
        """Initialize a new script.

        :param name: The name of the script. This may be a path.
        :type name: str

        :param factory: A factory instance used to automatic load commands
                       from a file.
        :type factory: commander.factory.Factory

        """
        self.name = name
        self.outputs = dict()
        self.results = dict()
        self._commands = list()

        if factory:
            if not factory.is_loaded:
                factory.load()

            for c in factory.get_commands():
                self.add(c)

    def __repr__(self):
        return "<%s %s>" % (self.__class__.__name__, self.name)

    def __str__(self):
        return self.to_string()

    def add(self, command, **kwargs):
        """Add a command to the script.

        :param command: The command to be added.
        :type command: Command | str

        :raise: TypeError

        """
        if isinstance(command, Command):
            self._commands.append(command)
        elif type(command) is str:
            command = Command(command, **kwargs)
            self._commands.append(command)
        else:
            message = "The given command is not an instance of Command or a str to be used as a Command: %s"
            raise TypeError(message % type(command))

    def get_commands(self):
        """Get the commands in the script.

        :rtype: list

        """
        return self._commands

    def run(self, stop=False):
        """Run the commands in the script.

        :param stop: Stop if any command fails.
        :type stop: bool

        :rtype: bool

        """
        commands = self.get_commands()
        overall_success = True
        for command in commands:
            success = command.run()

            self.outputs[command.uid] = command.output
            self.results[command.uid] = command.status

            if success:
                logger.info("Success: %s" % command.uid)
            else:
                overall_success = False

                if stop or command.stop:
                    logger.critical("Failure: %s" % command.uid)
                    break

                logger.warning("Failure: %s" % command.uid)

        return overall_success

    def to_string(self):
        """Export the script as a series of commands.

        :rtype: str

        """
        lines = list()
        lines.append("#! /usr/bin/env bash")
        lines.append("")

        for command in self.get_commands():
            if command.comment:
                lines.append("# %s" % command.comment)

            lines.append(command.preview())
            lines.append("")

        return "\n".join(lines)

    def write(self, path=None):
        """Write the script.

        :param path: If given, ``name`` is appended to the path.
        :type path: str

        :rtype: bool
        :returns: ``True`` if successful, ``False`` otherwise.

        """
        if path:
            path = os.path.join(path, self.name)
        else:
            path = self.name

        write_file(path, self.to_string())
