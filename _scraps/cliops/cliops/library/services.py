from .base import Command

# Classes


class Reload(Command):

    def __init__(self, service, **kwargs):
        name = "service %s reload" % service
        super().__init__(name, **kwargs)


class Restart(Command):

    def __init__(self, service, **kwargs):
        name = "service %s restart" % service
        super().__init__(name, **kwargs)


class Start(Command):
    """Start a service."""

    def __init__(self, service, **kwargs):
        """Stop the named service.

        :param service: The name of the service.
        :type service: str

        """
        name = "service %s start" % service

        super().__init__(name, **kwargs)


class Stop(Command):
    """Stop a service."""

    def __init__(self, service, **kwargs):
        """Stop the named service.

        :param service: The name of the service.
        :type service: str

        """
        name = "service %s stop" % service

        super().__init__(name, **kwargs)


MAPPING = {
    'reload': Reload,
    'restart': Restart,
    'start': Start,
    'stop': Stop,
}
