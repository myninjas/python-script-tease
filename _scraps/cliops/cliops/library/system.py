# Imports

from myninjas.platform import Platform
import os
import sys
from .base import Command

platform = Platform(sys)

# Exports

__all__ = (
    "MAPPING",
    "Append",
    "Copy",
    "MakeDir",
    "MakeDirIf",
    "Permissions",
    "Remove",
    "Rsync",
    "Sed",
    "Symlink",
    "Touch",
)

# Classes


class Append(Command):
    """Append to a file."""

    def __init__(self, target, **kwargs):
        """Append content to a file.

        :param target: The path to the file.
        :type target: str

        :param content: The content to be appended.
        :type content: str

        """
        content = kwargs.pop("content")

        name = 'echo "%s" >> %s' % (content, target)

        super().__init__(name, **kwargs)


class Copy(Command):

    def __init__(self, from_path, to_path, **kwargs):

        recursive = kwargs.pop("recursive", False)
        if recursive:
            name = "cp -R"
        else:
            name = "cp"

        name = "%s %s %s" % (name, from_path, to_path)
        super().__init__(name, **kwargs)


class MakeDir(Command):

    def __init__(self, target, **kwargs):
        self.mode = kwargs.pop("mode", None)
        self.recursive = kwargs.pop("recursive", False)

        tokens = ["mkdir"]

        # Windows is recursive by default.
        if "-p" in target:
            pass
        else:
            if self.mode is not None and not platform.is_windows:
                tokens.append("-m %s" % self.mode)

            if self.recursive and not platform.is_windows:
                tokens.append("-p")

        tokens.append(target)

        name = " ".join(tokens)

        super().__init__(name, **kwargs)


class MakeDirIf(MakeDir):

    def __init__(self, target, **kwargs):
        super().__init__(target, **kwargs)

        if platform.is_windows:
            name = 'if not exists "%s" %s' % (target, self.name)
        else:
            tokens = ['if [[ ! -d "%s" ]]; then mkdir']
            if self.recursive:
                tokens.append('-p')

            tokens.append('fi;')

            name = " ".join(tokens)

        self.name = name


class Permissions(Command):

    def __init__(self, target, **kwargs):
        commands = list()

        group = kwargs.pop("group", None)
        mode = kwargs.pop("mode", None)
        owner = kwargs.pop("owner", None)
        recursive = kwargs.pop("recursive", False)

        super().__init__("# permissions", **kwargs)

        if group is not None:
            tokens = ["chgrp"]

            if recursive:
                tokens.append("-R")

            tokens.append(group)

            tokens.append(target)

            commands.append(Command(" ".join(tokens), **kwargs))

        if owner is not None:
            tokens = ["chown"]

            if recursive:
                tokens.append("-R")

            tokens.append(owner)

            tokens.append(target)

            commands.append(Command(" ".join(tokens), **kwargs))

        if mode is not None:
            tokens = ["chmod"]

            if recursive:
                tokens.append("-R")

            tokens.append(str(mode))

            tokens.append(target)

            commands.append(Command(" ".join(tokens), **kwargs))

        self.commands = commands
        # print(commands)

    def preview(self):
        a = list()
        for c in self.commands:
            a.append(c.preview())

        return "\n".join(a)

    def run(self):
        results = list()
        for c in self.commands:
            results.append(c.run())

        return all(results)


class Remove(Command):

    def __init__(self, target, **kwargs):
        force = kwargs.pop("force", False)
        recursive = kwargs.pop("recursive", False)

        tokens = ["rm"]

        if force:
            tokens.append("-f")

        if recursive:
            tokens.append("-r")

        tokens.append(target)

        name = " ".join(tokens)

        super().__init__(name, **kwargs)


class Rsync(Command):

    def __init__(self, source, target, **kwargs):
        delete = kwargs.pop("delete", False)
        host = kwargs.pop("host", None)
        key_file = kwargs.pop("key_file", None)
        links = kwargs.pop("links", True)
        port = kwargs.pop("port", 22)
        recursive = kwargs.pop("recursive", True)
        user = kwargs.pop("user", None)

        # rsync -e "ssh -i $(SSH_KEY) -p $(SSH_PORT)" -P -rvzc --delete
        # $(OUTPUTH_PATH) $(SSH_USER)@$(SSH_HOST):$(UPLOAD_PATH) --cvs-exclude;

        if host is None:
            host = os.path.basename(source).replace("_", ".")

        if key_file is None:
            default_key_file = os.path.join("~/.ssh", os.path.basename(source))
            key_file = os.path.expanduser(default_key_file)

        if user is None:
            user = os.path.basename(source)

        tokens = list()
        tokens.append('rsync -e "ssh -i %s -p %s"' % (key_file, port))

        # --partial and --progress
        tokens.append("-P")

        tokens.append("--checksum")
        tokens.append("--compress")

        if links:
            tokens.append("--copy-links")

        if delete:
            tokens.append("--delete")

        if recursive:
            tokens.append("--recursive")

        tokens.append(source)

        tokens.append("%s@%s:%s" % (user, host, target))

        tokens.append("--cvs-exclude")

        name = " ".join(tokens)

        super().__init__(name, **kwargs)


class Sed(Command):

    def __init__(self, target, **kwargs):
        backup_extension = kwargs.pop("backup", ".b")
        delimiter = kwargs.pop("delimiter", "/")
        find = kwargs.pop("find")
        replace = kwargs.pop('replace')

        context = {
            'backup': backup_extension,
            'delimiter': delimiter,
            'pattern': find,
            'replace': replace,
            'path': target,
        }

        template = "sed -i %(backup)s 's%(delimiter)s%(pattern)s%(delimiter)s%(replace)s%(delimiter)sg' %(path)s"

        name = template % context

        super().__init__(name, **kwargs)


class Symlink(Command):

    def __init__(self, source, **kwargs):
        force = kwargs.pop("force", False)
        target = kwargs.pop("target", os.path.basename(source))

        if force:
            name = "ln -sf %s %s" % (source, target)
        else:
            name = "ln -s %s %s" % (source, target)

        super().__init__(name, **kwargs)


class Touch(Command):

    def __init__(self, target, **kwargs):
        items = kwargs.pop("items", None)

        if items is not None:
            self._commands = list()
            name = "# touch multiple targets"

            for i in items:
                _target = target.replace("$item", i)
                self._commands.append(Touch(_target, **kwargs))
        else:
            self._commands = None
            name = "touch %s" % target

        super().__init__(name, **kwargs)

    def get_command(self, include_path=False):
        if self._commands is not None:
            a = list()
            for c in self._commands:
                a.append(c.get_command(include_path=include_path))

            return "\n".join(a)

        return super().get_command(include_path=include_path)


class VirtualEnv(Command):

    def __init__(self, **kwargs):
        name = kwargs.pop("name", "python")

        super().__init__("virtualenv %s" % name, **kwargs)


MAPPING = {
    'alias': Symlink,
    'append': Append,
    'copy': Copy,
    'cp': Copy,
    'link': Symlink,
    'makedir': MakeDir,
    'makedirif': MakeDirIf,
    'mkdir': MakeDir,
    'mkdirif': MakeDirIf,
    'perm': Permissions,
    'permissions': Permissions,
    'perms': Permissions,
    'remove': Remove,
    'rm': Remove,
    'rsync': Rsync,
    'sed': Sed,
    'sync': Rsync,
    'symlink': Symlink,
    'touch': Touch,
    'venv': VirtualEnv,
    'virtualenv': VirtualEnv,
}
