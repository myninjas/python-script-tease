# Imports

import os
from .base import Command

# Exports

# Classes


class Archive(Command):

    def __init__(self, from_path, absolute=False, exclude=None, file_name="archive.tgz", strip=0, to_path=".",
                 view=False, **kwargs):
        """Create an archive file.

        :param from_path: The path that should be archived.
        :type from_path: str

        :param absolute: By default, the leading slash is stripped from each path. Set to ``True`` to preserve the
                         absolute path.
        :type absolute: bool

        :param bzip2: Compress using bzip2.
        :type bzip2: bool

        :param exclude: A pattern to be excluded from the archive.
        :type exclude: str

        :param format: The command to use for the operation.
        :type format: str

        :param gzip: Compress using gzip.
        :type gzip: bool

        :param strip: Remove the specified number of leading elements from the path. Paths with fewer elements will be
                      silently skipped.
        :type strip: int

        :param to_path: Where the archive should be created. This should *not* include the file name.
        :type to_path: str

        :param view: View the output of the command as it happens.
        :type view: bool

        """
        tokens = ["tar"]
        switches = ["-cz"]

        if absolute:
            switches.append("P")
        
        if view:
            switches.append("v")
            
        tokens.append("".join(switches))

        if exclude:
            tokens.append("--exclude %s" % exclude)

        if strip:
            tokens.append("--strip-components %s" % strip)

        to_path = os.path.join(to_path, file_name)
        tokens.append('-f %s %s' % (to_path, from_path))

        name = " ".join(tokens)

        super().__init__(name, **kwargs)


class Extract(Command):
    """Extract an archive."""

    def __init__(self, from_path, absolute=False, exclude=None, file_name="archive.tgz", strip=0, to_path=None,
                 view=False, **kwargs):
        """Extract an archive file.

        :param from_path: The path to the archive file.
        :type from_path: str

        :param absolute: By default, the leading slash is stripped from each path. Set to ``True`` to preserve the
                         absolute path.
        :type absolute: bool

        :param exclude: A pattern to be excluded from the archive.
        :type exclude: str

        :param strip: Remove the specified number of leading elements from the path. Paths with fewer elements will be
                      silently skipped.
        :type strip: int

        :param to_path: Where the archive should be extracted.
        :type to_path: str

        :param view: View the output of the command as it happens.
        :type view: bool

        """
        tokens = ["tar"]
        switches = ["-xz"]

        if absolute:
            switches.append("P")

        if view:
            switches.append("v")

        tokens.append("".join(switches))

        if exclude:
            tokens.append("--exclude %s" % exclude)

        if strip:
            tokens.append("--strip-components %s" % strip)

        to_path = os.path.join(to_path, file_name)
        tokens.append('-f %s %s' % (from_path, to_path))

        name = " ".join(tokens)

        super().__init__(name, **kwargs)


MAPPING = {
    'archive': Archive,
    'extract': Extract,
}
