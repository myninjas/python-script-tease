from .base import Command

# Classes


class Message(Command):

    def __init__(self, output, **kwargs):
        name = 'echo "%s"' % output
        super().__init__(name, **kwargs)


class Slack(object):

    def __init__(self, message, token, **kwargs):
        self.color = kwargs.pop("color", "good")
        self.message = message
        self.token = token
        self.url = "https://slack.com/url/to/be/determined"

    def preview(self):
        return self.url


MAPPING = {
    'echo': Message,
    'feedback': Message,
    'message': Message,
    'msg': Message,
    'slack': Slack,
}

