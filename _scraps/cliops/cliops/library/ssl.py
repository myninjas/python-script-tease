from .base import Command

# Classes


class Certbot(Command):

    def __init__(self, domain_name, **kwargs):
        domain_name = domain_name
        email = kwargs.pop("email")
        webroot = kwargs.pop("webroot")

        template = "certbot certonly --agree-tos --email %(email)s -n --webroot -w %(webroot)s -d %(domain_name)s"
        name = template % {
            'domain_name': domain_name,
            'email': email,
            'webroot': webroot,
        }

        super().__init__(name, **kwargs)


MAPPING = {
    'certbot': Certbot,
}
