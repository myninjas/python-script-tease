from .base import Command, ItemizedCommand

# Classes


class ConfigTest(Command):

    def __init__(self, **kwargs):
        name = "apachectl configtest"

        super().__init__(name, **kwargs)


class DisableModule(Command):

    def __init__(self, target, **kwargs):
        name = "a2dismod %s" % target

        super().__init__(name, **kwargs)


class DisableSite(Command):

    def __init__(self, target, **kwargs):
        name = "a2dissite %s" % target

        super().__init__(name, **kwargs)


class EnableModule(ItemizedCommand):

    def __init__(self, target, **kwargs):
        name = "a2enmod %s" % target

        super().__init__(name, **kwargs)

    def get_subcommands(self, name, **kwargs):
        if self.items is None:
            self._commands.append(Command(name, **kwargs))
            return

        for item in self.items:
            target = name.replace("$item", item)
            self._commands.append(Command(target, **kwargs))


class EnableSite(Command):

    def __init__(self, target, **kwargs):
        name = "a2ensite %s" % target

        super().__init__(name, **kwargs)


MAPPING = {
    'apache.config': ConfigTest,
    'apache.configtest': ConfigTest,
    'apache.disable_mod': DisableModule,
    'apache.disable_site': DisableSite,
    'apache.enable_mod': EnableModule,
    'apache.enable_module': EnableModule,
    'apache.enable_site': EnableSite,
    'apache.test': ConfigTest,
}
