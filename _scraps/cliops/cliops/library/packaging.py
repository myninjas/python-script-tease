# Imports

from myninjas.platform import Platform
import os
import sys
from .base import Command

platform = Platform(sys)

# Exports

# Classes


class Apt(Command):

    def __init__(self, target, **kwargs):
        name = "apt-get install -y %s" % target
        super().__init__(name, **kwargs)


class Install(Command):

    def __init__(self, *args, **kwargs):
        pass


class Pip(Command):

    def __init__(self, target, **kwargs):
        name = "pip3 install %s" % target
        super().__init__(name, **kwargs)


class Yum(Command):

    # http://man7.org/linux/man-pages/man8/yum.8.html

    def __init__(self, target, **kwargs):
        name = "yum -y install %s" % target
        super().__init__(name, **kwargs)


MAPPING = {
    'apt': Apt,
    'pip': Pip,
    'yum': Yum,
}
