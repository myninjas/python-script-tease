# create a directory that cannot be browsed

mkdir: /path/to/www  
recursive: yes
---

touch: /path/to/www/index.html

---

# enable an apache module


apache.enable_module: rewrite

---