# Imports
from fabric.api import puts
from fabric.colors import blue, green, red, yellow
from jinja2 import Template as JinjaTemplate
from string import Template as StringTemplate

# Functions


def jinja_render(template_path, context):
    """Render using Jinja 2.

    :param template_path: Path to the template file.
    :type template_path: str

    :param context: Context variables for the template.
    :type context: dict

    :rtype: str

    """

    # Load the template.
    with open(template_path, "rb") as f:
        template = JinjaTemplate(f.read())
        f.close()

    # Parse the template.
    return template.render(**context)


def string_render(template_path, context):
    """Render using Python's string formatting.

    :param template_path: Path to the template file.
    :type template_path: str

    :param context: Context variables for the template.
    :type context: dict

    :rtype: str

    .. note::
        Context variables must be converted to single words, and string_render()
        handles this on the fly. However, you template must accept these words
        instead of the dot notification. For example: ``project_name`` instead
        of ``project.name``.

    """

    # Load the template.
    with open(template_path, "rb") as f:
        template = StringTemplate(f.read())
        f.close()

    # Convert context names.
    _context = dict()
    for section, obj in context.items():
        for key, value in obj:
            name = "%s_%s" % (section, key)
            _context[name] = value

    # Parse the template.
    return template.strip().format(**_context)


def write_file(content, path):
    """Write (replace) the content of a file.

    :param content: The content to be written.
    :type content: str

    :param path: The path to be written.
    :type path: str

    """
    with open(path, "wb") as f:
        f.write(content)
        f.close()


# Feedback Functions


class Feedback(object):

    @classmethod
    def failure(cls, message, context=None):
        cls._print(message, color=red, context=context)

    @classmethod
    def info(cls, message, context=None):
        cls._print(message, color=blue, context=context)

    @classmethod
    def success(cls, message, context=None):
        cls._print(message, color=green, context=context)

    @classmethod
    def warning(cls, message, context=None):
        cls._print(message, color=yellow, context=context)

    @classmethod
    def _print(cls, message, color=None, context=None):
        if context:
            message = message % context

        if color:
            message = color(message)

        puts(message)

