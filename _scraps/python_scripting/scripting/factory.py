# Imports
from ConfigParser import RawConfigParser
from importlib import import_module
import os
from slugify import slugify
from .base import *

# Functions


def get_class(class_name):
    """Get the class for the given class name.

    :param class_name: The name of the class.
    :type class_name: str

    :rtype: object
    :returns: Returns the class, or ``Shell`` by default.

    """
    try:
        return globals()[class_name]
    except KeyError:

        # The module we want to load is determined by removing the class name from
        # the end of the name string.
        tokens = class_name.split(".")
        module_name = ".".join(tokens[:-1])

        # Now try to load the module.
        module = get_module(module_name)

        # If the module was loaded, we'll attempt to return the class.
        class_name = tokens[-1]
        try:
            return getattr(module, class_name)
        except TypeError:
            return Shell
        except AttributeError:
            return Shell


def get_command(class_name, *args, **kwargs):
    """Instantiate a command class.

    :param class_name: The name of the class.
    :type class_name: str

    :rtype: object
    :returns: Returns the class, or ``Shell`` by default.

    .. note::
        ``args`` and ``kwargs`` for the class may be passed to the function.
        However, all parameters may be passed in ``kwargs``, which allows
        instantiation to occur at runtime using, for example, configuration
        data.

    """
    cls = get_class(class_name)
    return cls(*args, **kwargs)


def get_module(name):
    """Import a module and return it.

    :param name: The name of the module.
    :type name: str

    :rtype: object

    .. note::
        We first try to load the module as is, which provides some basic
        support for pluggable behavior. If that fails, ``scripting.`` is
        prepended to the ``name`` and we try the import again. If that fails,
        ``None`` is returned.

    """
    try:
        return import_module(name)
    except ImportError:
        try:
            return import_module("scripting.%s" % name)
        except ImportError:
            return None


# Classes


class Factory(object):
    """Allows commands to be loaded from a configuration file."""

    def __init__(self, path, context=None, debug=False, name=None):
        """Initialize a scripting configuration.

        :param path: Path to the INI file.
        :type path: str

        :param context: Context variables.
        :type context: dict

        :param debug: Enable debug mode during ``load()``.
        :type debug: bool

        :param name: The name of the configuration. If omitted, the file name is
                     used.
        :type name: str

        .. note::
            The configuration is not loaded as part of instantiation. See
            ``load()``.

        """
        self.context = context
        self.debug = debug
        self.is_loaded = False
        self.path = path
        self._commands = list()

        if name:
            self.name = name
        else:
            file_name, extension = os.path.basename(path).split(".")
            self.name = file_name

    def get_commands(self):
        """Get the commands loaded from configuration data.

        :rtype: list

        """
        return self._commands

    def load(self):
        """Load the configuration data."""

        # Load the config without interpolation.
        ini = RawConfigParser()
        ini.read(self.path)

        # Add each command to the instance.
        class_name = "Shell"
        for section in ini.sections():

            kwargs = {
                'comment': section,
                'context': self.context,
            }
            for key, value in ini.items(section):
                if key in ("cmd", "cls", "do"):
                    class_name = value
                else:
                    kwargs[key] = value

            if self.debug:
                print "[%s]" % section
                print "    %s: %s" % (class_name, kwargs)

            obj = get_command(class_name, **kwargs)
            # obj.preview()

            if not obj.uid:
                obj.uid = str(slugify(u"%s" % section))

            self._commands.append(obj)

        self.is_loaded = True
