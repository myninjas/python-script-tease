from .base import Shell


from .base import Shell

# Commands


class Mysql(Shell):
    """Base command for working with MySQL commands."""

    def to_string(self):
        """MySQL commands do *not* use sudo."""
        return self._get_command()

    def _get_base_command(self, name="mysql"):
        """Get the base command.

        :param name: The command name.
        :type name: str

        :rtype: str

        """
        a = list()
        a.append("%s --user=%s" % (name, self.admin))

        if self.host:
            a.append("--host=%s" % self.host)

        if self.port:
            a.append("--port=%s" % self.port)

        if self.admin_password:
            a.append("--password=%s" % self.admin_password)

        return " ".join(a)


class CreateDatabase(Mysql):

    def __init__(self, database, admin="root", admin_password=None, host="localhost", owner=None, port=None, **kwargs):
        """Create a database.

        :param database: The database name.
        :type database: str

        :param admin: The admin user.
        :type admin: str

        :param admin_password: The password for the admin user.
        :type admin_password: str

        :param host: Host name.
        :type host: str

        :param owner: User that will own the database. This user must already
                      exist. See ``CreateUser``.
        :type owner: str

        :param port: TCP port.
        :type: port: int

        .. note::
            Additional keyword arguments are passed to the underlying
            ``Shell`` instance.

        """
        self.admin = admin
        self.database = database
        self.host = host
        self.owner = owner
        self.admin_password = admin_password
        self.port = port

        super(CreateDatabase, self).__init__(**kwargs)

    def _get_command(self):

        # Assemble the base command.
        a = list()
        a.append(self._get_base_command(name="mysqladmin"))
        a.append("create %s" % self.database)

        # Identify the database owner if given.
        if self.owner:
            grant = GrantPrivileges(
                self.database,
                self.owner,
                admin=self.admin,
                admin_password=self.admin_password,
                host=self.host,
                port=self.port
            )
            a.append("&& %s" % grant.to_string())

        # The default command is created.
        command = " ".join(a)

        return command


class CreateUser(Mysql):

    def __init__(self, user_name, admin="root", admin_password=None, host="localhost", password=None, port=None, **kwargs):
        """Create a user.

        :param user_name: The user name.
        :type user_name: str

        :param admin: The admin user.
        :type admin: str

        :param admin_password: The password for the admin user.
        :type admin_password: str

        :param host: Host name.
        :type host: str

        :param password: The user's password.
        :type password: str

        :param port: TCP port.
        :type: port: int

        .. note::
            Additional keyword arguments are passed to the underlying
            ``Shell`` instance.

        """
        self.admin = admin
        self.admin_password = admin_password
        self.host = host
        self.password = password
        self.port = port
        self.user_name = user_name

        super(CreateUser, self).__init__(**kwargs)

    def _get_command(self):

        # Assemble the SQL.
        sql = "CREATE USER '%(user)s'@'%(host)s'" % {
            'host': self.host,
            'user': self.user_name,
        }

        if self.password:
            sql += " IDENTIFIED BY PASSWORD('%s')" % self.password

        # Assemble the command.
        a = list()
        a.append(self._get_base_command())
        a.append('--execute="%s"' % sql)

        # Create the command.
        command = " ".join(a)

        return command


class ExportDatabase(Mysql):

    def __init__(self, database, admin="root", admin_password=None, column_inserts=False, data_only=False, host="localhost", path=None, port=None, schema_only=False, **kwargs):
        """Prepare a database dump.

        :param database: The database name.
        :type database: str

        :param admin: The admin user.
        :type admin: str

        :param admin_password: The password for the admin user.
        :type admin_password: str

        :param column_inserts: Dump the data as ``INSERT`` commands with the
                               corresponding column names.
        :type column_inserts: bool

        :param data_only: Dump the data and not the schema.
        :type data_only: bool

        :param host: Host name.
        :type host: str

        :param path: If given, the dump will be written to this file.
        :type path: str

        :param port: TCP port.
        :type: port: int

        :param schema_only: Dump the schema and not the data.
        :type schema_only: bool

        .. note::
            Additional keyword arguments are passed to the underlying
            ``Shell`` instance.

        .. note::
            The same method signature is used here, though ``mysqldump`` does
            not have the same switches as ``pg_dump``. The equivalent switches
            are used in the output of the command.

        """
        self.admin = admin
        self.column_inserts = column_inserts
        self.data_only = data_only
        self.database = database
        self.admin_password = admin_password
        self.host = host
        self.path = path
        self.port = port
        self.schema_only = schema_only

        super(ExportDatabase, self).__init__(**kwargs)

    def _get_command(self):

        # Assemble the base command.
        a = list()
        a.append(self._get_base_command(name="mysqldump"))

        # Create the command.
        if self.data_only:
            a.append('--no-create-info')
        elif self.schema_only:
            a.append('--no-data')
        else:
            pass

        if self.column_inserts:
            a.append('--complete-insert')

        if self.path:
            a.append("> %s" % self.path)

        a.append(self.database)

        command = " ".join(a)

        return command


class GrantPrivileges(Mysql):
    """Grant privileges to a user.

    See https://dev.mysql.com/doc/refman/5.7/en/grant.html

    """

    def __init__(self, database, user_name, admin="root", admin_password=None, host="localhost", port=None, privileges="ALL", **kwargs):
        """Create a user.

        :param database: The name of the database.
        :type database: str

        :param user_name: The user name.
        :type user_name: str

        :param admin: The admin user.
        :type admin: str

        :param admin_password: The password for the admin user.
        :type admin_password: str

        :param host: Host name.
        :type host: str

        :param password: The user's password.
        :type password: str

        :param port: TCP port.
        :type: port: int

        :param privileges: The privileges to be granted.
        :type privileges: str

        .. note::
            Additional keyword arguments are passed to the underlying
            ``Shell`` instance.

        """
        self.admin = admin
        self.admin_password = admin_password
        self.database = database
        self.host = host
        self.port = port
        self.privileges = privileges
        self.user_name = user_name

        super(GrantPrivileges, self).__init__(**kwargs)

    def _get_command(self):
        a = list()
        a.append(self._get_base_command())

        sql = "GRANT %(privileges)s ON %(database)s.* TO '%(user)s'@'%(host)s'" % {
            'database': self.database,
            'host': self.host,
            'privileges': self.privileges,
            'user': self.user_name,
        }

        a.append('--execute="%s"' % sql)

        return " ".join(a)

# Exports

__all__ = [
    "CreateDatabase",
    "CreateUser",
    "ExportDatabase",
    "GrantPrivileges",
]