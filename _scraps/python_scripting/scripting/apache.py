# Imports
from .base import Shell

# Commands


class Module(Shell):
    """Work with Apache modules."""

    def __init__(self, name, operation="enable", **kwargs):
        self.name = name
        self.operation = operation
        super(Module, self).__init__(**kwargs)

    def _get_command(self):
        if self.operation == "enable":
            return "a2enmode %s" % self.name
        elif self.operation == "disable":
            return "a2dismod %s" % self.name
        else:
            raise ValueError("Unsupported operation: %s" % self.operation)


class Site(Shell):
    """Work with Apache sites."""

    def __init__(self, name, operation="enable", **kwargs):
        self.name = name
        self.operation = operation
        super(Site, self).__init__(**kwargs)

    def _get_command(self):
        if self.operation == "enable":
            return "a2ensite %s" % self.name
        elif self.operation == "disable":
            return "a2dissite %s" % self.name
        else:
            raise ValueError("Unsupported operation: %s" % self.operation)


class DisableModule(Shell):
    """Disable a module."""

    def __init__(self, module, **kwargs):
        """Disable a module.

        :param module: The name of the module.
        :type module: str

        .. tip::
            Sudo is not enabled by default, but is almost certainly required.

        """
        self.module = module

        super(DisableModule, self).__init__(**kwargs)

    def _get_command(self):
        return "a2dismod %s" % self.module


class DisableSite(Shell):
    """Disable a site."""

    def __init__(self, site, **kwargs):
        """Disable a site.

        :param site: The name of the site.
        :type site: str

        .. tip::
            Sudo is not enabled by default, but is almost certainly required.

        """
        self.site = site

        super(DisableSite, self).__init__(**kwargs)

    def _get_command(self):
        return "a2dissite %s" % self.site


class EnableModule(Shell):
    """Enable a module."""

    def __init__(self, module, **kwargs):
        """Enable a module.

        :param module: The name of the module.
        :type module: str

        .. tip::
            Sudo is not enabled by default, but is almost certainly required.

        """
        self.module = module

        super(EnableModule, self).__init__(**kwargs)

    def _get_command(self):
        return "a2enmod %s" % self.module


class EnableSite(Shell):
    """Enable a site."""

    def __init__(self, site, **kwargs):
        """Enable a site.

        :param site: The name of the site.
        :type site: str

        .. tip::
            Sudo is not enabled by default, but is almost certainly required.

        """
        self.site = site

        super(EnableSite, self).__init__(**kwargs)

    def _get_command(self):
        return "a2ensite %s" % self.site