# Imports
from commands import getstatusoutput
from fabric.api import put, puts as feedback, run, sudo
from fabric.colors import blue, green, red, yellow
from fabric.contrib.files import append, upload_template
from fabric.utils import indent
import os
from .constants import EXIT_IO, EXIT_OK, EXIT_UNKNOWN
from .utils import Feedback, jinja_render, write_file

# Classes


class Script(object):
    """A script is a collection of commands."""

    def __init__(self, name, factory=None):
        """Initialize a new script.

        :param name: The name of the script. This may be a path.
        :type name: str

        :param factory: A factory instance used to automatic load commands
                       from a file.
        :type factory: factory.Factory

        """
        self.name = name
        self.outputs = dict()
        self.results = dict()
        self._commands = list()

        if factory:
            if not factory.is_loaded:
                factory.load()

            for c in factory.get_commands():
                self.add(c)

    def __str__(self):
        return self.to_string()

    def add(self, command):
        """Add a command to the script.

        :param command: The command to be added.
        :type command: Shell

        """
        self._commands.append(command)

    def get_commands(self):
        """Get the commands in the script.

        :rtype: list

        """
        return self._commands

    def run(self, stop=False):
        """Run the commands in the script.

        :param stop: Stop if any command fails.
        :type stop: bool

        :rtype: bool

        """
        commands = self.get_commands()
        overall_success = True
        for command in commands:
            success = command.run()

            self.outputs[command.uid] = command.output
            self.results[command.uid] = command.status

            if success:
                Feedback.success("Success: %s" % command.uid)
                print ""
            else:
                overall_success = False

                Feedback.failure("Failure: %s" % command.uid)
                Feedback.warning(indent(command.output))
                # print red("Failure: %s" % command.uid)
                # print yellow(indent(command.output))
                print ""

                if stop:
                    break

        return overall_success

    def to_string(self):
        """Export the script as a series of commands.

        :rtype: str

        """
        lines = list()
        lines.append("#! /usr/bin/env bash")
        lines.append("")

        for command in self.get_commands():
            if command.comment:
                lines.append("# %s" % command.comment)

            lines.append(command.to_string())
            lines.append("")

        return "\n".join(lines)

    def write(self, path=None):
        """Write the script.

        :param path: If given, ``name`` is appended to the path.
        :type path: str

        :rtype: bool
        :returns: ``True`` if successful, ``False`` otherwise.

        """
        if path:
            path = os.path.join(path, self.name)
        else:
            path = self.name

        try:
            with open(path, "wb") as f:
                f.write(self.to_string())
                f.close()
            return True
        except IOError:
            return False

# Commands


class Shell(object):
    """Basic shell command. Extend as needed."""

    def __init__(self, command=None, comment=None, context=None, enabled=True, extras=None, remote=False, sudo=False, template=None, uid=None):
        """A generic shell command.

        :param command: The command to be executed. If given, ``context`` and
                        ``template`` are ignored. Additionally, no interpolation
                        is applied to the command. If omitted, the attribute is
                        automatically set when executing ``local()`` or
                        ``remote()``.
        :type command: str

        :param comment: Optional comment.
        :type comment: str

        :param context: Values to parse into the command using string
                       interpolation.
        :type context: dict

        :param enabled: Whether the command is enabled for execution. This is
                        useful when controlling commands programmatically at
                        run time.
        :type enabled: bool

        :param extras: Extra arguments or switches to be passed as is to the
                       underlying command.
        :type extras: str

        :param id: A unique ID for the command.
        :type id: str

        :param remote: Run the command using ``remote()``. By default,
                       ``local()`` is used.
        :type remote: bool

        :param sudo: Run the command using sudo. May be given as a user name
                    (``str``) or boolean.
        :type sudo: bool|str

        :param template: The command to be executed. This may be provided as a
                         template for use with Python's string interpolation.
        :type template: str

        **Using Sudo**

        When ``True``, remote commands are executed with the Fabric's
        ``env.sudo_user`` or the default for the remote system -- usually root.

        ``Shell`` is used as the basis for all other commands, and these
        may or may not support sudo as described here. Differences are noted.

        """
        self.command = command
        self.comment = comment
        self.context = context or dict()
        self.enabled = enabled
        self.extras = extras
        self.output = None
        self.status = EXIT_UNKNOWN
        self.sudo_user = None
        self.template = template
        self.uid = uid

        if isinstance(sudo, str):
            self.sudo_user = sudo
            self.sudo = True
        else:
            self.sudo = sudo

        if remote:
            self.is_local = False
            self.is_remote = True
        else:
            self.is_local = True
            self.is_remote = False

    def __str__(self):
        return self.to_string()

    def local(self):
        """Run the command on the local machine.

        :rtype: bool
        :returns: Returns ``True`` if the exit code is normal, ``False``
                  otherwise. Both ``status`` and ``output`` are captured as
                  properties.

        """
        self.command = self.to_string()

        (status, output) = getstatusoutput(self.command)
        self.status = status
        self.output = output

        if status == 0:
            return True
        else:
            return False

    def preview(self):
        """Preview the command rather than executing it.

        :rtype: str

        """
        a = list()
        if self.comment:
            a.append("# %s" % self.comment)

        a.append(self.to_string())
        a.append("")

        return "\n".join(a)

    def remote(self):
        """Run the command on a remote machine using Fabric's ``run()``
        function.

        :rtype: bool
        :returns: Returns ``True`` if the exit code is normal, ``False``
                  otherwise. Both ``status`` and ``output`` are captured as
                  properties.

        .. note::
            This method does *not* use ``to_string()`` because Fabric's
            ``sudo()`` function is used.

        """
        command = self._get_command()

        if self.sudo:
            result = sudo(command, user=self.sudo_user)
        else:
            result = run(command)

        self.command = result.real_command
        self.output = result
        self.status = result.return_code

        if self.status == EXIT_OK:
            return True
        else:
            return False

    def run(self):
        """Return the command using the appropriate method; ``local()`` or
        ``remote()``.

        :rtype: bool

        """
        if self.is_remote:
            return self.remote()
        else:
            return self.local()

    def to_string(self):
        """Get the full command (including sudo) as a string. Comment is not
        included.

        :rtype: str

        """
        if self.sudo:
            return "%s %s" % (self._get_sudo(), self._get_command())
        else:
            return self._get_command()

    def _get_command(self):
        """Get the command that will be executed.

        :rtype: str

        """
        if self.command:
            return self.command

        if self.context:
            return self.template % self.context
        elif self.template:
            return self.template
        else:
            raise RuntimeError("A command or template and context is required for: %s" % self.__class__.__name__)

    def _get_sudo(self):
        """Get the sudo prefix, if any.

        :rtype: str

        """
        a = list()

        if self.sudo:
            if self.sudo_user:
                a.append("sudo -u %s" % self.sudo_user)
            else:
                a.append("sudo")

        return " ".join(a)


class Append(Shell):
    """Append to a file."""

    def __init__(self, content, path, **kwargs):
        """Append content to a file.

        :param content: The content to be appended.
        :type content: str

        :param path: The path to the file. Context is parsed into this path.
        :type path: str

        """
        self.content = content
        self.path = path

        super(Append, self).__init__(**kwargs)

    def remote(self):
        """Append to the file using Fabric's ``append()`` function.

        .. note::
            This method does *not* use ``get_command()``.

        .. warning:
            ``append()`` does not support a sudo user, defaulting to
            ``env.sudo_user``. So you'll want to set this on ``env`` if sudo is
            required.

        """
        # TODO: Determine the errors that can occur with append() and trap them
        # to return True or False. See Shell.remote().
        append(
            self._get_path(),
            self.content,
            use_sudo=self.sudo
        )

        return True

    def _get_command(self):
        a = list()
        a.append("cat >> %s << EOF" % self._get_path())
        a.append(self.content)
        a.append("EOF")

        return "\n".join(a)

    def _get_path(self):
        if self.context:
            return self.path % self.context
        else:
            return self.path


class Apt(Shell):
    """Install a package using apt."""

    def __init__(self, package, version=None, **kwargs):
        """Install a package.

        :param package: The name of the package to be installed.
        :type package: str

        :param version: The version of the package to be installed.
        :type version: str

        .. note::
            Additional keyword arguments are passed to the underlying
            ``Shell`` instance.

        """
        self.package = package
        self.version = version

        super(Apt, self).__init__(**kwargs)

    def _get_command(self):
        a = list()
        a.append("apt-get install -y %s" % self.package)

        if self.version:
            a.append("=%s", self.version)

        return "".join(a)


class Archive(Shell):
    """Create an archive."""

    def __init__(self, from_location, absolute=False, bzip2=False, exclude=None, format="tar", gzip=False, strip=0, to_location=None, view=False, **kwargs):
        """Create an archive file.

        :param from_location: The path that should be archived.
        :type from_location: str

        :param absolute: By default, the leading slash is stripped from each
                         path. Set to ``True`` to preserve the absolute path.
        :type absolute: bool

        :param bzip2: Compress using bzip2.
        :type bzip2: bool

        :param exclude: A pattern to be excluded from the archive.
        :type exclude: str

        :param format: The command to use for the operation.
        :type format: str

        :param gzip: Compress using gzip.
        :type gzip: bool

        :param strip: Remove the specified number of leading elements from the
                      path. Paths with fewer elements will be silently skipped.
        :type strip: int

        :param to_location: Where the archive should be created. This should
                            include the file name, i.e. ``archive.tar``.
        :type to_location: str

        :param view: View the output of the command as it happens.
        :type view: bool

        .. note::
            Context is parsed into both ``from_location`` and ``to_location``.

        .. note::
            Additional keyword arguments are passed to the underlying
            ``Shell`` instance.

        """
        self.absolute = absolute
        self.bzip2 = bzip2
        self.exclude = exclude
        self.format = format
        self.from_location = from_location
        self.gzip = gzip
        self.strip = strip
        self.to_location = to_location
        self.view = view

        super(Archive, self).__init__(**kwargs)

    @property
    def dirname(self):
        """Get the path just above the from location.

        :rtype: str

        """
        return os.path.dirname(self._get_from_location())

    def _get_command(self):
        if self.format == "tar":
            return self._get_tar_command()
        else:
            raise ValueError("Unsupported archive format: %s" % self.format)

    def _get_file_name(self):
        """Automatically determine the file name of the archive.

        :rtype: str

        """
        if self.to_location:
            return os.path.basename(self.to_location)
        elif self.bzip2:
            return "archive.tbz"
        elif self.gzip:
            return "archive.tgz"
        else:
            return "archive.tar"

    def _get_from_location(self):
        """Get the path to be archived with context.

        :rtype: str

        """
        if self.context:
            return self.from_location % self.context
        else:
            return self.from_location

    def _get_to_location(self):
        """Get the destination path with context.

        :rtype: str

        """
        if self.to_location:
            if self.context:
                return self.to_location % self.context
            else:
                return self.to_location
        else:
            return "%s/%s" % (self.dirname, self._get_file_name())

    def _get_tar_command(self):
        command = "tar -c"

        if self.view:
            command += "v"

        if self.absolute:
            command += "P"

        if self.bzip2:
            command += "j"
        elif self.gzip:
            command += "z"
        else:
            pass

        if self.strip > 0:
            command += " --strip-components %s" % self.strip

        if self.exclude:
            command += " --exclude %s" % self.exclude

        if self.extras:
            command += " " + self.extras

        command += " -f %s %s" % (self._get_to_location(), self._get_from_location())

        return command


class Cat(Shell):
    """Cat a file."""

    def __init__(self, path, content=None, **kwargs):
        """Cat a file, and optionally, write the file.

        :param path: The path to the file.
        :type path: str

        :param content: The content of the file.
        :type content: str

        .. note::
            Values in the ``context`` kwarg will be parsed into ``content``
            and ``path``.

        .. note::
            Additional keyword arguments are passed to the underlying
            ``Shell`` instance.

        """
        self.content = content
        self.path = path

        super(Cat, self).__init__(**kwargs)

    def _get_command(self):

        if self.content:
            a = list()
            a.append("cat > %s << EOF" % self._get_path())

            if self.context:
                a.append(self.content % self.context)
            else:
                a.append(self.content)

            a.append("EOF")

            return "\n".join(a)
        else:
            return "cat %s" % self._get_path()

    def _get_path(self):
        """Get the path with context

        :rtype: str

        """
        if self.context:
            return self.path % self.context
        else:
            return self.path


class Copy(Shell):
    """Copy files and directories."""

    def __init__(self, from_location, to_location, mode=None, **kwargs):
        """Copy a file or directory from one location to another.

        :param from_location: The path to the source or original.
        :type from_location: str

        :param mode: Set this mode after the copy.
        :type mode: str

        :param to_location: The destination path.
        :type to_location: str

        .. note::
            Context is parsed into ``to_location``.

        .. note::
            Additional keyword arguments are passed to the underlying
            ``Shell`` instance.

        """
        self.from_location = from_location
        self.mode = mode
        self.to_location = to_location

        super(Copy, self).__init__(**kwargs)

    @property
    def is_directory(self):
        """Determine whether a directory is being copied.

        :rtype: bool

        .. note::
            We assume that the local path always exists regardless of whether
            the command runs locally, on a remote, or locally to a remote. So it
            is safe to determine if the from_location is a directory.

        """
        return os.path.isdir(self.from_location)

    def remote(self):
        """Copy the file or directory using Fabric's ``put()`` function.

        .. note::
            This method does *not* use ``get_command()``.

        .. warning:
            ``put()`` does not support a sudo user, defaulting to
            ``env.sudo_user``. So you'll want to set this on ``env`` if sudo is
            required.

        """
        if self.context:
            to_location = self.to_location % self.context
        else:
            to_location = self.to_location

        if self.sudo:
            result = put(self.from_location, to_location, mode=self.mode, use_sudo=True)
        else:
            result = put(self.from_location, to_location, mode=self.mode)

        self.status = result.return_code
        self.output = result

        if self.status == EXIT_OK:
            return True
        else:
            return False

    def _get_command(self):
        a = list()

        a.append("cp")

        if self.is_directory:
            a.append("-R")

        a.append(self.from_location)

        if self.context:
            to_location = self.to_location % self.context
        else:
            to_location = self.to_location

        a.append(to_location)

        if self.mode:
            a.append("&& chmod")

            if self.is_directory:
                a.append("-R")

            a.append(self.mode)

        return " ".join(a)


class Echo(Shell):
    """Echo output or feedback."""

    def __init__(self, message, color=None, line=None, **kwargs):
        """Echo a message.

        :param message: The message to echo.
        :type message: str

        :param color: The color to use. One of: blue, green, red, or yellow.
        :type color: str

        :param line: Underline the output with this character.
        :type line: str

        .. note::
            Additional keyword arguments are passed to the underlying
            ``Shell`` instance.

        .. note::
            Currently, color is only supported for ``local()`` and ``remote()``
            execution.

        .. note::
            ``message`` supports ``context`` variables.

        """
        self.line = line
        self.message = message

        if color == "blue":
            self.color = blue
        elif color == "green":
            self.color = green
        elif color == "red":
            self.color = red
        elif color == "yellow":
            self.color = yellow
        else:
            self.color = None

        super(Echo, self).__init__(**kwargs)

    def local(self):
        """Print the message using Fabric's ``puts()`` function."""
        message = self._get_message()

        if self.color:
            color = self.color
            message = color(message)

        feedback(message)

        if self.line:
            feedback(self._get_line())

    def remote(self):
        """Remote and local are the same, but we include the method so that
        run time automation will work either way.

        """
        return self.local()

    def _get_command(self):
        message = self._get_message()

        a = list()
        a.append('echo "%s"' % message)

        if self.line:
            a.append('&& echo "%s"' % self._get_line())

        return " ".join(a)

    def _get_line(self):
        """Get the line to echo after the main message.

        :rtype: str

        """
        if self.line:
            return self.line * len(self._get_message())
        else:
            return ""

    def _get_message(self):
        """Get the message with context.

        :rtype: str

        """
        if self.context:
            return self.message % self.context
        else:
            return self.message


class Extract(Shell):
    """Extract an archive."""

    def __init__(self, from_location, absolute=False, bzip2=False, exclude=None, format="tar", gzip=False, strip=0, to_location=None, view=False, **kwargs):
        """Extract an archive file.

        :param from_location: The path to the archive file.
        :type from_location: str

        :param absolute: By default, the leading slash is stripped from each
                         path. Set to ``True`` to preserve the absolute path.
        :type absolute: bool

        :param bzip2: Decompress using bzip2.
        :type bzip2: bool

        :param exclude: A pattern to be excluded from the archive.
        :type exclude: str

        :param format: The command to use for the operation.
        :type format: str

        :param gzip: Decompress using gzip.
        :type gzip: bool

        :param strip: Remove the specified number of leading elements from the
                      path. Paths with fewer elements will be silently skipped.
        :type strip: int

        :param to_location: Where the archive should be extracted.
        :type to_location: str

        :param view: View the output of the command as it happens.
        :type view: bool

        .. note::
            Context is parsed into both ``from_location`` and ``to_location``.

        .. note::
            Additional keyword arguments are passed to the underlying
            ``Shell`` instance.

        """
        self.absolute = absolute
        self.bzip2 = bzip2
        self.exclude = exclude
        self.format = format
        self.from_location = from_location
        self.gzip = gzip
        self.strip = strip
        self.to_location = to_location
        self.view = view

        super(Extract, self).__init__(**kwargs)

    @property
    def dirname(self):
        """Get the path just above the from location.

        :rtype: str

        """
        return os.path.dirname(self._get_from_location())

    def _get_command(self):
        if self.format == "tar":
            return self._get_tar_command()
        else:
            raise ValueError("Unsupported archive format: %s" % self.format)

    def _get_file_name(self):
        """Automatically determine the file name of the archive.

        :rtype: str

        """
        if self.to_location:
            return os.path.basename(self.to_location)
        elif self.bzip2:
            return "archive.tbz"
        elif self.gzip:
            return "archive.tgz"
        else:
            return "archive.tar"

    def _get_from_location(self):
        """Get the path to be archived with context.

        :rtype: str

        """
        if self.context:
            return self.from_location % self.context
        else:
            return self.from_location

    def _get_to_location(self):
        """Get the destination path with context.

        :rtype: str

        """
        if self.to_location:
            if self.context:
                return self.to_location % self.context
            else:
                return self.to_location
        else:
            return "%s/%s" % (self.dirname, self._get_file_name())

    def _get_tar_command(self):
        command = "tar -x"

        if self.view:
            command += "v"

        if self.absolute:
            command += "P"

        if self.bzip2:
            command += "j"
        elif self.gzip:
            command += "z"
        else:
            pass

        if self.strip > 0:
            command += " --strip-components %s" % self.strip

        if self.exclude:
            command += " --exclude %s" % self.exclude

        if self.extras:
            command += " " + self.extras

        command += " -f %s %s" % (self._get_from_location(), self._get_to_location())

        return command


class InstallPackage(Shell):
    """A basic command for installing software packages."""

    def __init__(self, package, manager="pip", version=None, **kwargs):
        """Install a package.

        :param package: The name of the package to be installed.
        :type package: str

        :param manager: The package manager to use.
        :type manager: str

        :param version: The version of the package to be installed.
        :type version: str

        .. note::
            Additional keyword arguments are passed to the underlying
            ``Shell`` instance.

        """
        self.manager = manager
        self.package = package
        self.version = version

        super(InstallPackage, self).__init__(**kwargs)

    def _get_command(self):
        if self.manager == "apt":
            return self._get_apt_command()
        elif self.manager == "pip":
            return self._get_pip_command()
        else:
            raise ValueError("Un-recognized package manager for InstallPackage: %s" % self.manager)

    def _get_apt_command(self):
        a = list()
        a.append("apt-get install -y %s" % self.package)

        if self.version:
            a.append("=%s", self.version)

        return "".join(a)

    def _get_pip_command(self):
        a = list()
        a.append("pip install %s" % self.package)

        if self.version:
            a.append(self._get_sign())

        return "".join(a)

    def _get_sign(self):
        for sign in ("<", ">", "="):
            if sign in self.version:
                return self.version

        return "==%s" % self.version


class MakeDir(Shell):

    def __init__(self, path, mode=None, recursive=True, **kwargs):
        """Create a directory.

        :param path: The path to the directory. User expansion is performed
                     automatically.
        :type path: str

        :param mode: The mode to use for the directory.
        :type mode: mode

        :param recursive: Whether directories along the path should also be
                          created. Defaults to ``True``.
        :type recursive: bool

        .. note::
            Additional keyword arguments are passed to the underlying
            ``Shell`` instance.

        """
        self.mode = mode
        self.path = path
        self.recursive = recursive

        super(MakeDir, self).__init__(**kwargs)

    def _get_command(self):
        a = list()
        a.append("mkdir")

        if self.recursive:
            a.append("-p")

        if self.mode:
            a.append("-m %s" % self.mode)

        a.append(self._get_path())

        return " ".join(a)

    def _get_path(self):
        """Get the path with context.

        :rtype: str

        """
        if self.context:
            return self.path % self.context
        else:
            return self.path


class Permissions(Shell):
    """Manage file permissions."""

    def __init__(self, path, group=None, mode=None, owner=None, recursive=True, **kwargs):
        """Assign permissions to a file or directory.

        :param path: The path to the file or directory. Context is parsed.
        :type path: str

        :param group: The user group.
        :type group: str

        :param mode: File mode specifying the permissions.
        :type mode: str

        :param owner: The owner of the file or directory.
        :type owner: str

        :param recursive: Set permissions recursively on directories.
        :type recursive: bool

        """
        self.group = group
        self.mode = mode
        self.owner = owner
        self.path = path
        self.recursive = recursive

        super(Permissions, self).__init__(**kwargs)

    def _get_base_command(self, name="chmod"):
        if self.recursive:
            return "%s -R" % name
        else:
            return name

    def _get_command(self):
        a = list()

        if self.group:
            a.append("%s %s" % (self._get_base_command(name="chgrp"), self.group))

        if self.owner:
            a.append("%s %s" % (self._get_base_command(name="chown"), self.owner))

        if self.mode:
            a.append("%s %s" % (self._get_base_command(), self.mode))

        return "\n".join(a)

    def _get_path(self):
        """Get the path with context.

        :rtype: str

        """
        if self.context:
            return self.path % self.context
        else:
            return self.path


class Pip(Shell):
    """Install a Python package using pip."""

    def __init__(self, package, version=None, **kwargs):
        """Install a package.

        :param package: The name of the package to be installed.
        :type package: str

        :param version: The version of the package to be installed.
        :type version: str

        .. note::
            Additional keyword arguments are passed to the underlying
            ``Shell`` instance.

        """
        self.package = package
        self.version = version

        super(Pip, self).__init__(**kwargs)

    def _get_command(self):
        a = list()
        a.append("pip install %s" % self.package)

        if self.version:
            a.append(self._get_sign())

        return "".join(a)

    def _get_sign(self):
        for sign in ("<", ">", "="):
            if sign in self.version:
                return self.version

        return "==%s" % self.version


class Reload(Shell):
    """Reload a service."""

    def __init__(self, service, **kwargs):
        """Reload the named service.

        :param service: The name of the service.
        :type service: str

        """
        self.service = service

        super(Reload, self).__init__(**kwargs)

    def _get_command(self):
        return "service %s reload" % self.service


class Service(Shell):
    """Work with a server service."""

    def __init__(self, service, operation, **kwargs):
        """Work with the named service.

        :param service: The name of the service.
        :type service: str

        :param operation: The operation to peform. For example, reload or stop.
        :type operation: str

        .. note::
            Use of sudo is almost certainly required, but is not enabled by
            default.

        """
        self.operation = operation
        self.service = service

        super(Service, self).__init__(**kwargs)

    def _get_command(self):
        return "service %s %s" % (self.service, self.operation)


class Stop(Shell):
    """Stop a service."""

    def __init__(self, service, **kwargs):
        """Stop the named service.

        :param service: The name of the service.
        :type service: str

        """
        self.service = service

        super(Stop, self).__init__(**kwargs)

    def _get_command(self):
        return "service %s reload" % self.service


class Symlink(Shell):

    def __init__(self, from_location, to_location, **kwargs):
        """Create a symlink.

        :param from_location: The path where the symlink should be created.
        :type from_location: str

        :param to_location: The file, directory or path being linked to at the given
                       location.
        :type to_location: str

        .. note::
            ``from_location`` and ``to_location`` are both parsed for context.

        .. note::
            Additional keyword arguments are passed to the underlying
            ``Shell`` instance.

        """
        self.from_location = from_location
        self.to_location = to_location

        super(Symlink, self).__init__(**kwargs)

    def _get_command(self):
        if self.context:
            from_location = self.from_location % self.context
            to_location = self.to_location % self.context
        else:
            from_location = self.from_location
            to_location = self.to_location

        return "cd %s && ln -s %s" % (from_location, to_location)


class Template(Shell):
    """Parse a template."""

    def __init__(self, from_location, to_location, group=None, mode=None, owner=None, **kwargs):
        """Parse a template and output the result to a given location.

        :param from_location: The path to the template.
        :type from_location: str

        :param to_location: The path to the parsed file.
        :type to_location: str

        :param group: User group to assign.
        :type group: str

        :param mode: The file mode to set for the parsed file.
        :type mode: str

        :param owner: User to assign.
        :type owner: str

        .. note::
            Because ``context`` is part of ``ShellCommands`` keyword arguments,
            we must manually check for the presence of context variables before
            executing the command.

            This is not done when initializing the object to allow the setting
            of context after the instance has been created.

        """
        self.from_location = from_location
        self.group = group
        self.mode = mode
        self.owner = owner
        self.to_location = to_location

        super(Template, self).__init__(**kwargs)

    def local(self):
        context = self._get_context()

        try:
            self.output = jinja_render(self._get_from_location(), context)
            write_file(self.output, self._get_to_location())
            self.status = EXIT_OK
        except IOError:
            self.status = EXIT_IO

        if self.status == EXIT_OK:
            return True
        else:
            return False

    def remote(self):
        context = self._get_context()

        result = upload_template(
            self._get_from_location(),
            self._get_to_location(),
            context=context,
            mode=self.mode,
            use_jinja=True,
            use_sudo=self.sudo
        )
        self.output = result

        if result.succeeded:
            self.status = EXIT_OK
            return True
        else:
            self.status = EXIT_IO
            return False

    def _get_command(self):
        """Easily parsing a template requires a higher-level programming than
        Bash. This method returns the command as a series of ``sed`` statements.
        Because escaping can be tricky, there is no guarantee that these will
        work, or even be valid bash. For this reason, ``local()`` and
        ``remote()`` do *not* use this method.
        """
        context = self._get_context()

        a = list()

        # Make a copy of the file.
        from_location = self._get_from_location()
        base_name = os.path.basename(from_location)
        a.append('cp %s /tmp/%s' % (from_location, base_name))

        # Set up find and replace on the file.
        for key, value in context.items():
           a.append("sed -i backup -e 's,%s,%s,g' /tmp/%s" %(key, value, base_name))

        # Move the file.
        to_location = self._get_to_location()
        a.append("mv /tmp/%s %s" %(base_name, to_location))

        # Set permissions.
        if self.group or self.mode or self.owner:
            command = Permissions(to_location, group=self.group, mode=self.mode, owner=self.owner, recursive=False)
            a.append(command.to_string())

        return "\n".join(a)

    def _get_context(self):
        """Get the context dictionary, or raise a ``ValueError`` if no context
        has been provided.

        :rtype: dict

        """
        if isinstance(self.context, dict):
            return self.context
        else:
            raise ValueError("Context dictionary is required for: %s" % self.__class__.__name__)

    def _get_from_location(self):
        """Get the from path with context.

        :rtype: str

        """
        if self.context:
            return self.from_location % self.context
        else:
            return self.from_location

    def _get_to_location(self):
        """Get the to path with context.

        :rtype: str

        """
        if self.context:
            return self.to_location % self.context
        else:
            return self.to_location


class Touch(Shell):

    def __init__(self, path, **kwargs):
        """Create a symlink.

        :param path: The path to be touched.
        :type path: str

        .. note::
            Additional keyword arguments are passed to the underlying
            ``Shell`` instance.

        """
        self.path = path

        super(Touch, self).__init__(**kwargs)

    def _get_command(self):
        return "touch %s" % self._get_path()

    def _get_path(self):
        """Get the path with context.

        :rtype: str

        """
        if self.context:
            return self.path % self.context
        else:
            return self.path


class Write(Shell):
    """Write a file."""

    def __init__(self, content, path, **kwargs):
        """Write to a file using the ``cat`` command.

        :param content: The content of the file.
        :type content: str

        :param path: The path to the file.
        :type path: str

        .. note::
            Values in the ``context`` kwarg will be parsed into ``content``
            and ``path``.

        .. note::
            Additional keyword arguments are passed to the underlying
            ``Shell`` instance.

        """
        self.content = content
        self.path = path

        super(Write, self).__init__(**kwargs)

    def _get_command(self):

        if self.context:
            content = self.content % self.context
        else:
            content = self.content

        lines = list()
        lines.append("cat > %s << EOF" % self._get_path())
        lines.append(content)
        lines.append("EOF")

        return "\n".join(lines)

    def _get_path(self):
        """Get the path with context

        :rtype: str

        """
        if self.context:
            return self.path % self.context
        else:
            return self.path


# TODO: Package management commands for Brew, Gem, Npm.

# TODO: SCM commands for Git, Hg, and Svn.

# Exports

__all__ = [
    "Append",
    "Apt",
    "Archive",
    "Cat",
    "Copy",
    "Echo",
    "Extract",
    "InstallPackage",
    "MakeDir",
    "Permissions",
    "Pip",
    "Reload",
    "Service",
    "Shell",
    "Stop",
    "Symlink",
    "Template",
    "Touch",
    "Write",
]