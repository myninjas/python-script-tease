from .base import Shell

# Commands


class Pgsql(Shell):
    """Base command for working with Postgres commands."""

    def to_string(self):
        """Postgres commands do *not* use sudo."""
        return self._get_command()

    def _get_base_command(self, name="psql"):
        """Get the base command.

        :param name: The command name.
        :type name: str

        :rtype: str

        """
        a = list()
        a.append("%s -U %s" % (name, self.admin))

        if self.host:
            a.append("--host=%s" % self.host)

        if self.port:
            a.append("--port=%s" % self.port)

        return " ".join(a)


class CreateDatabase(Pgsql):

    def __init__(self, database, admin="postgres", admin_password=None, host="localhost", owner=None, port=None, **kwargs):
        """Create a database.

        :param database: The database name.
        :type database: str

        :param admin: The admin user.
        :type admin: str

        :param admin_password: The password for the admin user.
        :type admin_password: str

        :param host: Host name.
        :type host: str

        :param owner: User that will own the database. This user must already
                      exist. See ``CreateUser``.
        :type owner: str

        :param port: TCP port.
        :type: port: int

        .. note::
            Additional keyword arguments are passed to the underlying
            ``Shell`` instance.

        """
        self.admin = admin
        self.database = database
        self.host = host
        self.owner = owner
        self.admin_password = admin_password
        self.port = port

        super(CreateDatabase, self).__init__(**kwargs)

    def _get_command(self):

        # Assemble the base command.
        a = list()
        a.append(self._get_base_command(name="createdb"))

        # Identify the database owner if given.
        if self.owner:
            a.append("--owner %s" % self.owner)

        # Don't forget to include the database name.
        a.append(self.database)

        # The default command is created.
        command = " ".join(a)

        # Postgres needs special handling for passwords. I don't see how this
        # approach is really more secure than a password switch since we are
        # all forced to "export PG_PASSWORD"?
        if self.admin_password:
            command = 'export PG_PASSWORD="%s" && %s' % (self.admin_password, command)

        return command


class CreateUser(Pgsql):

    def __init__(self, user_name, admin="postgres", admin_password=None, host="localhost", password=None, port=None, **kwargs):
        """Create a user (role).

        :param user_name: The user name.
        :type user_name: str

        :param admin: The admin user.
        :type admin: str

        :param admin_password: The password for the admin user.
        :type admin_password: str

        :param host: Host name.
        :type host: str

        :param owner: User that will own the database. This user must already
                      exist. See ``CreateUser``.
        :type owner: str

        :param port: TCP port.
        :type: port: int

        .. note::
            Additional keyword arguments are passed to the underlying
            ``Shell`` instance.

        """
        self.admin = admin
        self.admin_password = admin_password
        self.host = host
        self.password = password
        self.port = port
        self.user_name = user_name

        super(CreateUser, self).__init__(**kwargs)

    def _get_command(self):

        # Assemble the SQL.
        sql = list()
        sql.append("CREATE USER %s" % self.user_name)

        if self.password:
            sql.append("WITH ENCRYPTED PASSWORD '%s'" % self.password)

        # Assemble the base command.
        a = list()
        a.append(self._get_base_command())

        # Create the command.
        command = '%s -c "%s";' % (" ".join(a), " ".join(sql))

        # Deal with postgres password.
        if self.admin_password:
            command = 'export PG_PASSWORD="%s" && %s' % (self.admin_password, command)

        return command


class ExportDatabase(Pgsql):

    def __init__(self, database, admin="postgres", admin_password=None, column_inserts=False, data_only=False, host="localhost", path=None, port=None, schema_only=False, **kwargs):
        """Prepare a database dump.

        :param database: The database name.
        :type database: str

        :param admin: The admin user.
        :type admin: str

        :param admin_password: The password for the admin user.
        :type admin_password: str

        :param column_inserts: Dump the data as ``INSERT`` commands with the
                               corresponding column names.
        :type column_inserts: bool

        :param data_only: Dump the data and not the schema.
        :type data_only: bool

        :param host: Host name.
        :type host: str

        :param path: If given, the dump will be written to this file.
        :type path: str

        :param port: TCP port.
        :type: port: int

        :param schema_only: Dump the schema and not the data.
        :type schema_only: bool

        .. note::
            Additional keyword arguments are passed to the underlying
            ``Shell`` instance.

        """
        self.admin = admin
        self.column_inserts = column_inserts
        self.data_only = data_only
        self.database = database
        self.admin_password = admin_password
        self.host = host
        self.path = path
        self.port = port
        self.schema_only = schema_only

        super(ExportDatabase, self).__init__(**kwargs)

    def _get_command(self):

        # Assemble the base command.
        a = list()
        a.append(self._get_base_command(name="pg_dump"))

        # Create the command.
        if self.data_only:
            a.append('--data-only')
        elif self.schema_only:
            a.append('--schema-only')
        else:
            pass

        if self.column_inserts:
            a.append('--column-inserts')

        if self.path:
            a.append("--file=%s" % self.path)

        a.append(self.database)

        command = " ".join(a)

        # Deal with postgres password.
        if self.admin_password:
            command = 'export PG_PASSWORD="%s" && %s' % (
            self.admin_password, command)

        return command


class GrantPrivileges(Pgsql):
    """Grant privileges to a user.

    See http://www.postgresql.org/docs/current/static/sql-grant.html

    """

    def __init__(self, database, table, user_name, admin="postgres", admin_password=None, host="localhost", port=None, privileges="ALL PRIVILEGES", **kwargs):
        """Create a user.

        :param database: The name of the database.
        :type database: str

        :param table: The name of the table.
        :type table: str

        :param user_name: The user name receiving the privileges.
        :type user_name: str

        :param admin: The admin user.
        :type admin: str

        :param admin_password: The password for the admin user.
        :type admin_password: str

        :param host: Host name.
        :type host: str

        :param password: The user's password.
        :type password: str

        :param port: TCP port.
        :type: port: int

        :param privileges: The privileges to be granted.
        :type privileges: str

        .. note::
            Additional keyword arguments are passed to the underlying
            ``Shell`` instance.

        .. note::
            Grant for Postgres operates on tables while grant for MySQL operates
            on a database.

        """
        self.admin = admin
        self.admin_password = admin_password
        self.database = database
        self.host = host
        self.port = port
        self.privileges = privileges
        self.table = table
        self.user_name = user_name

        super(GrantPrivileges, self).__init__(**kwargs)

    def _get_command(self):
        a = list()
        a.append(self._get_base_command())
        a.append(self.database)

        sql = "GRANT %(privileges)s ON %(table)s TO %(user)s" % {
            'privileges': self.privileges,
            'table': self.table,
            'user': self.user_name,
        }

        a.append('-c "%s"' % sql)

        return " ".join(a)


class InstallExtension(Pgsql):
    """Install one or more PostgreSQL extensions."""

    def __init__(self, extension, admin="postgres", admin_password=None, database="template1", host="localhost", port=None, **kwargs):
        """Install an extension.

        :param extension: The extension name.
        :type extension: str

        :param admin: The admin user.
        :type admin: str

        :param admin_password: The password for the admin user.
        :type admin_password: str

        :param database: The database name.
        :type database: str

        :param host: Host name.
        :type host: str

        :param owner: User that will own the database. This user must already
                      exist. See ``CreateUser``.
        :type owner: str

        :param port: TCP port.
        :type: port: int

        .. note::
            Additional keyword arguments are passed to the underlying
            ``Shell`` instance.

        """
        self.admin = admin
        self.admin_password = admin_password
        self.database = database
        self.extension = extension
        self.host = host
        self.port = port

        super(InstallExtension, self).__init__(**kwargs)

    def _get_command(self):
        a = list()
        a.append(self._get_base_command())
        a.append(self.database)
        a.append('-c "CREATE EXTENSION IF NOT EXISTS %s"' % self.extension)
        return " ".join(a)



# Exports

__all__ = [
    "CreateDatabase",
    "CreateUser",
    "ExportDatabase",
    "GrantPrivileges",
    "InstallExtension",
]