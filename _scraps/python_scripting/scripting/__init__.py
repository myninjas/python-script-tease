from ConfigParser import RawConfigParser
import os
from slugify import slugify
from .factory import Factory, get_class, get_command, get_module
from .base import Append, Apt, Archive, Cat, Copy, Echo, Extract, InstallPackage, MakeDir, Permissions, Pip, Reload, Script, Service, Shell, Stop, Symlink, Template, Touch, Write



class Configuration(object):
    """Allows commands to be loaded from a configuration file."""

    def __init__(self, path, context=None, debug=False, name=None):
        """Initialize a scripting configuration.

        :param path: Path to the INI file.
        :type path: str

        :param context: Context variables.
        :type context: dict

        :param debug: Enable debug mode during ``load()``.
        :type debug: bool

        :param name: The name of the configuration. If omitted, the file name is
                     used.
        :type name: str

        .. note::
            The configuration is not loaded as part of instantiation. See
            ``load()``.

        """
        self.context = context
        self.debug = debug
        self.path = path
        self._commands = list()

        if name:
            self.name = name
        else:
            file_name, extension = os.path.basename(path).split(".")
            self.name = file_name

    def get_commands(self):
        """Get the commands loaded from configuration data.

        :rtype: list

        """
        return self._commands

    def load(self):
        """Load the configuration data."""

        # Load the config without interpolation.
        ini = RawConfigParser()
        ini.read(self.path)

        # Add each command to the instance.
        class_name = "Shell"
        for section in ini.sections():

            kwargs = {
                'comment': section,
                'context': self.context,
            }
            for key, value in ini.items(section):
                if key in ("cmd", "cls", "do"):
                    class_name = value
                else:
                    kwargs[key] = value

            if self.debug:
                print "[%s]" % section
                print "   ", kwargs
                print "   ", class_name

            obj = get_command(class_name, **kwargs)
            # obj.preview()

            if not obj.uid:
                obj.uid = slugify(u"%s" % section)

            self._commands.append(obj)


__all__ = [
    "Append",
    "Apt",
    "Archive",
    "Cat",
    "Configuration",
    "Copy",
    "Echo",
    "Extract",
    "Factory",
    "InstallPackage",
    "MakeDir",
    "Permissions",
    "Pip",
    "Reload",
    "Script",
    "Service",
    "Shell",
    "Stop",
    "Symlink",
    "Template",
    "Touch",
    "Write",
]