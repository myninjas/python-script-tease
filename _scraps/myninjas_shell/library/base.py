# Import

# import copy
# import logging
from myninjas.utils import is_bool, to_bool
# import os
import subprocess
import sys
# from myninjas.logging.constants import LOGGER_NAME as DEFAULT_LOGGER_NAME
from ..constants import EXIT_FAILURE, EXIT_OK, EXIT_UNKNOWN

# LOGGER_NAME = os.environ.get("MYNINJAS_LOGGER_NAME", DEFAULT_LOGGER_NAME)

# logger = logging.getLogger(LOGGER_NAME)

# Exports

__all__ = (
    "MAPPING",
    "abort",
    "Command",
    "ItemizedCommand",
)

# Functions


def abort(message, code=EXIT_FAILURE):
    """Stop processing and issue an exit/return code.

    :param message: A message to send to the user.
    :type message: str

    :param code: The exit code to issue.
    :type code: int

    .. code-block:: python

        from myninjas.shell import abort

        abort("I can't go on.")

    """
    if message:
        print(message)

    sys.exit(code)

# Classes


class Command(object):
    """Run a shell command.

    """

    def __init__(self, name, comment=None, environments=None, path=None, prefix=None, scope=None, shell=None,
                 stop=False, sudo=False):
        """Initialize a command.

        :param name: The command itself.
        :type name: str

        :param comment: The comment on the command.
        :type comment: str

        :param environments: Not used.
        :type environments: list[str]

        :param path: The path from which the command should be executed.
        :type path: str

        :param prefix: A pre-command such as export or virtual env call to be executed (after the path) before the
                       command.
        :type prefix: str

        :param scope: Not used.
        :type scope: str

        :param shell: The shell to use, for example: ``/bin/bash``
        :type shell: str

        :param stop: Indicates no additional commands should be executed if this command fails.
        :type stop: bool

        :param sudo: Indicates sudo should be used to execute the command
        :type sudo: bool | str

        .. note::
            The ``stop`` argument is *not* used by the command instance, but may be used in scripting to determine if
            additional commands should be executed.

        .. code-block:: python

            from myninjas.shell import Command

            # A simple command.
            command = Command("ls -ls")
            print(command.preview())

            if command.run():
                print("Your listing is above.")

            # Create /tmp/tmp.txt file.
            command = Command("touch tmp.txt", path="/tmp")
            command.run()

            # Using a prefix.
            command = Command("pg_dumpall -U postgres", prefix='export PGPASSWORD="mypassword"')
            command.run()

        """
        self.code = None
        self.comment = comment
        self.environments = environments
        self.error = None
        self.name = name
        self.output = None
        self.path = path
        self.prefix = prefix
        self.scope = scope
        self.shell = shell
        self.stop = stop

        if is_bool(sudo):
            self.sudo = to_bool(sudo)
        else:
            self.sudo = sudo

    def __repr__(self):
        return "<%s: %s>" % (self.__class__.__name__, self.name)

    def __str__(self):
        return self.get_command(include_path=True)

    def get_command(self, include_path=False):
        """Get the command to be executed with prefix and path.

        :param include_path: Indicates whether the path should be included. Generally speaking, you want to include the
                             path to preview the command and omit the path when you want to run the command.
        :type include_path: bool

        :rtype: str

        """
        a = list()

        if self.path and include_path:
            a.append("(cd " + self.path)

        if self.prefix:
            a.append(self.prefix)

        if self.sudo:
            if type(self.sudo) is str:
                a.append("sudo -u %s %s" % (self.sudo, self.name))
            else:
                a.append("sudo %s" % self.name)
        else:
            a.append(self.name)

        if self.path and include_path:
            return " && ".join(a) + ")"

        return " && ".join(a)

    def preview(self):
        """Get a preview of the command that will be executed.

        :rtype: str

        """
        return self.get_command(include_path=True)

    def run(self):
        """Run the command.

        :rtype: bool
        :returns: Returns ``True`` if the command was successful. The ``code`` attribute is also set.

        .. tip::
            Success depends upon the exit code of the command which is not available from all commands on all platforms.
            Check the command's documentation for exit codes and plan accordingly.

        """
        # Prepare to shell output.
        output_stream = subprocess.PIPE
        error_stream = subprocess.PIPE

        # The command without the path (if any) because path is passed to Popen via the cwd parameter.
        command = self.get_command()

        # Run the command. Capture output, error, and return code.
        try:
            p = subprocess.Popen(
                command,
                cwd=self.path,
                executable=self.shell,
                shell=True,
                stderr=error_stream,
                stdout=output_stream
            )
            (stdout, stderr) = p.communicate()

            self.code = p.returncode

            a = list()
            for line in str(stderr).split("\\n"):
                a.append(line.strip())

            self.error = "\n".join(a)

            a = list()
            for line in str(stdout).split("\\n"):
                a.append(line.strip())

            self.output = "\n".join(a)
        except Exception as e:
            self.code = EXIT_UNKNOWN
            self.error = str(e)

        return self.code == EXIT_OK


# class ItemizedCommand(Command):
#     """A command that includes multiple sub-commands.
#
#     .. code-block:: python
#
#         from myninjas.shell import ItemizedCommand
#
#         items = ["index.html", "content/index.html"]
#
#         command = ItemizedCommand("touch /path/to/www/$item", items=items)
#         print(command.preview())
#
#     """
#
#     def __init__(self, name, **kwargs):
#         """Initialize the commands.
#
#         :param name: The name of the command.
#         :type name: str
#
#         """
#         self.items = kwargs.pop("items", None)
#         self._commands = list()
#
#         super().__init__("# multiple %s" % name, **kwargs)
#
#         self.get_subcommands(name, **kwargs)
#
#     def get_command(self, include_path=False):
#         a = list()
#         for c in self._commands:
#             a.append(c.get_command(include_path=include_path))
#
#         return a
#
#     def get_subcommands(self, name, **kwargs):
#         raise NotImplementedError()
#
#     def preview(self):
#         a = list()
#         for c in self._commands:
#             a.append(c.preview())
#
#         return "\n".join(a)


class ItemizedCommand(object):
    """A command that includes multiple sub-commands.

    This class is intended to be extended by commands in the library, but may be used directly like so:

    .. code-block:: python

        from myninjas.shell import ItemizedCommand
        from myninjas.shell.library.system import Touch

        args = ["/path/to/www/$item"]
        items = ["index.html", "content/index.html"]

        command = ItemizedCommand(Touch, items, *args)
        print(command.preview())

    """
    def __init__(self, command, items, *args, **kwargs):
        self.args = args
        self.command = command
        self.items = items
        self.kwargs = kwargs
        self._commands = list()

        # Command attributes.
        self.code = None
        self.comment = kwargs.get("comment", None)
        self.environments = kwargs.get("environments", None)
        self.error = None
        self.name = command.__name__
        self.output = None
        self.path = kwargs.get("path", None)
        self.prefix = kwargs.get("prefix", None)
        self.scope = kwargs.get("scope", None)
        self.shell = kwargs.get("shell", None)
        self.stop = kwargs.get("stop", False)

        sudo = kwargs.get("sudo", False)
        if is_bool(sudo):
            self.sudo = to_bool(sudo)
        else:
            self.sudo = sudo

    def __repr__(self):
        return "<%s: %s>" % (self.__class__.__name__, self.name)

    def __str__(self):
        return self.preview()

    def get_command(self, include_path=False):
        """Preserve the interface for normal/single commands."""
        a = list()
        for c in self.get_commands():
            a.append(c.get_command(include_path=include_path))

        return "\n".join(a)

    def get_commands(self):
        """Get the commands to be executed.

        :rtype: list[BaseType[Command]]

        """
        # Return the results of previous calls to the method.
        if len(self._commands) > 0:
            return self._commands

        _kwargs = self.kwargs.copy()
        for item in self.items:
            _args = list()
            for a in self.args:
                a = a.replace("$item", item)
                _args.append(a)

            command = self.command(*_args, **_kwargs)
            self._commands.append(command)

        return self._commands

    def preview(self):
        """Get a preview of the command that will be executed.

        :rtype: str

        """
        a = list()
        for c in self.get_commands():
            a.append(c.get_command(include_path=True))

        return "\n".join(a)

    def run(self):
        """Run the command.

        :rtype: bool
        :returns: Returns ``True`` if the command was successful. The ``code`` attribute is also set.

        .. tip::
            Success depends upon the exit code of the command which is not available from all commands on all platforms.
            Check the command's documentation for exit codes and plan accordingly.

        """
        errors = list()
        outputs = list()
        results = list()

        commands = self.get_commands()
        for c in commands:
            results.append(c.run())
            errors.append(c.error)
            outputs.append(c.output)

        self.error = "\n".join(errors)
        self.output = "\n".join(outputs)

        if all(results):
            self.code = 0
            return True

        self.code = 1
        return False


MAPPING = {
    'do': Command,
    'command': Command,
    'cmd': Command,
    'run': Command,
}
