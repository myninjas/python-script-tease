"""
Messages
........

Classes for creating feedback and sending messages.

"""
# Imports

from .base import Command

# Exports

__all__ = (
    "Message",
    "Slack",
)

# Classes


class Message(Command):
    """Echo a message to the console."""

    def __init__(self, output, **kwargs):
        name = 'echo "%s"' % output
        super().__init__(name, **kwargs)


class Slack(object):
    """(NOT IMPLEMENTED) Post a message to Slack."""

    def __init__(self, message, token, **kwargs):
        self.color = kwargs.pop("color", "good")
        self.message = message
        self.token = token
        self.url = "https://slack.com/url/to/be/determined"

        # TODO: Implement the Slack command.

    def preview(self):
        """There's nothing to see here."""
        return self.url


MAPPING = {
    'echo': Message,
    'feedback': Message,
    'message': Message,
    'msg': Message,
    'slack': Slack,
}

