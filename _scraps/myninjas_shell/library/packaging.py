"""
Packaging
.........

Classes for working with install commands.

"""
# Imports

from .base import Command

# Exports

__all__ = (
    "Apt",
    "Pip",
    "Yum",
)

# Classes


class Apt(Command):
    """Install a package using apt-get."""

    def __init__(self, target, **kwargs):
        name = "apt-get install -y %s" % target
        super().__init__(name, **kwargs)


# class Install(Command):
#
#     def __init__(self, *args, **kwargs):
#         pass


class Pip(Command):
    """Install a Python package using pip."""

    def __init__(self, target, **kwargs):
        name = "pip3 install %s" % target
        super().__init__(name, **kwargs)


class Yum(Command):
    """Install a package using yum."""

    # http://man7.org/linux/man-pages/man8/yum.8.html

    def __init__(self, target, **kwargs):
        name = "yum -y install %s" % target
        super().__init__(name, **kwargs)


MAPPING = {
    'apt': Apt,
    'pip': Pip,
    'yum': Yum,
}
