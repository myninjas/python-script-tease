"""
Users
.....

Classes for working with user accounts.

"""
# Imports

from .base import Command

# Exports

__all__ = (
    "AddUser",
)

# Classes


class AddUser(Command):
    """Add a user account."""

    def __init__(self, name, groups=None, home=None, **kwargs):
        # Save name groups for get_command().
        self._groups = groups
        self._user_name = name

        # The gecos switch eliminates the prompts.
        tokens = ['adduser %s --disable-password --gecos ""' % name]
        if home is not None:
            tokens.append("--home %s" % home)

        target = " ".join(tokens)

        super().__init__(target, **kwargs)

    def get_command(self, include_path=False):
        a = list()

        command = super().get_command(include_path=include_path)
        a.append(command)

        if self._groups is not None:
            for group in self._groups:
                a.append("adduser %s %s" % (self._user_name, group))

            if "sudo" in self._groups:
                a.append('echo "%s ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/%s' % (self._user_name, self._user_name))

        return "\n".join(a)


MAPPING = {
    'adduser': AddUser,
}
