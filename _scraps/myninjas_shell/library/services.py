"""
Services
........

Classes for working with commands related to starting and stopping server services.

"""
# Imports

from .base import Command

# Exports

__all__ = (
    "Reload",
    "Restart",
    "Start",
    "Stop",
)

# Classes


class Reload(Command):
    """Reload a service."""

    def __init__(self, service, **kwargs):
        name = "service %s reload" % service
        super().__init__(name, **kwargs)


class Restart(Command):
    """Restart a service."""

    def __init__(self, service, **kwargs):
        name = "service %s restart" % service
        super().__init__(name, **kwargs)


class Start(Command):
    """Start a service."""

    def __init__(self, service, **kwargs):
        """Stop the named service.

        :param service: The name of the service.
        :type service: str

        """
        name = "service %s start" % service

        super().__init__(name, **kwargs)


class Stop(Command):
    """Stop a service."""

    def __init__(self, service, **kwargs):
        """Stop the named service.

        :param service: The name of the service.
        :type service: str

        """
        name = "service %s stop" % service

        super().__init__(name, **kwargs)


MAPPING = {
    'reload': Reload,
    'restart': Restart,
    'start': Start,
    'stop': Stop,
}
