# Imports

import os
from .base import Command

# Exports

__all__ = (
    "MAPPING",
    "Append",
    "Copy",
    "MakeDir",
    "Move",
    "Permissions",
    "Remove",
    "Rsync",
    "SCopy",
    "Sed",
    "Symlink",
    "Touch",
    "Write",
)

# Classes


class Append(Command):
    """Append to a file."""

    def __init__(self, path, content=None, **kwargs):
        """Append content to a file.

        :param path: The path to the file.
        :type path: str

        :param content: The content to be appended.
        :type content: str

        """
        self.content = content or ""
        self.path = path

        kwargs.setdefault("comment", "append to %s" % path)

        statement = 'echo "%s" >> %s' % (self.content, self.path)

        super().__init__(statement, **kwargs)


class Copy(Command):
    """Copy a file or directory."""

    def __init__(self, from_path, to_path, overwrite=False, recursive=False, **kwargs):
        """Initialize the command.

        :param from_path: The file or directory to be copied.
        :type from_path: str

        :param to_path: The location to which the file or directory should be copied.
        :type to_path: str

        :param overwrite: Indicates files and directories should be overwritten if they exist.
        :type overwrite: bool

        :param recursive: Copy sub-directories.
        :type recursive: bool

        """
        self.from_path = from_path
        self.overwrite = overwrite
        self.recursive = recursive
        self.to_path = to_path

        kwargs.setdefault("comment", "copy %s to %s" % (from_path, to_path))

        a = list()
        a.append("cp")

        if not overwrite:
            a.append("-n")

        if recursive:
            a.append("-R")

        a.append(from_path)
        a.append(to_path)

        super().__init__(" ".join(a), **kwargs)


class MakeDir(Command):
    """Create a directory."""

    def __init__(self, path, mode=None, recursive=True, **kwargs):
        """Initialize the command.

        :param path: The path to be created.
        :type path: str

        :param mode: The access permissions of the new directory.
        :type mode: str

        :param recursive: Create all directories along the path.
        :type recursive: bool

        """
        self.mode = mode
        self.path = path
        self.recursive = recursive

        kwargs.setdefault("comment", "make %s" % path)

        statement = ["mkdir"]
        if self.mode is not None:
            statement.append("-m %s" % self.mode)

        if self.recursive:
            statement.append("-p")

        statement.append(path)

        super().__init__(" ".join(statement), **kwargs)


class Move(Command):
    """Move a file or directory."""

    def __init__(self, from_path, to_path, **kwargs):
        self.from_path = from_path
        self.to_path = to_path

        statement = "mv %s %s" % (from_path, to_path)

        super().__init__(statement, **kwargs)


class Permissions(Command):
    """Set permissions on a file or directory."""

    def __init__(self, path, group=None, mode=None, owner=None, recursive=False, **kwargs):
        """Initialize the command.

        :param path: The path to be changed.
        :type path: str

        :param group: The name of the group to be applied.
        :type group: str

        :param mode: The access permissions of the file or directory.
        :type mode: str

        :param owner: The name of the user to be applied.
        :type owner: str

        :param recursive: Create all directories along the path.
        :type recursive: bool

        """
        self.group = group
        self.mode = mode
        self.owner = owner
        self.path = path
        self.recursive = recursive

        kwargs.setdefault("comment", "set permissions on %s" % path)

        super().__init__("", **kwargs)

        commands = list()
        if self.group is not None:
            statement = ["chgrp"]

            if self.recursive:
                statement.append("-R")

            statement.append(self.group)
            statement.append(self.path)

            commands.append(Command(" ".join(statement), **kwargs))

        if self.owner is not None:
            statement = ["chown"]

            if self.recursive:
                statement.append("-R")

            statement.append(self.owner)
            statement.append(self.path)

            commands.append(Command(" ".join(statement), **kwargs))

        if self.mode is not None:
            statement = ["chmod"]

            if self.recursive:
                statement.append("-R")

            statement.append(str(self.mode))
            statement.append(self.path)

            commands.append(Command(" ".join(statement), **kwargs))

        self._commands = commands

    def get_statement(self, cwd=False):
        a = list()
        a.append("# %s" % self.comment)

        for c in self._commands:
            c.comment = None
            a.append(c.get_statement(cwd=cwd))

        return "\n".join(a)


class Remove(Command):
    """Remove a file or directory."""

    def __init__(self, path, force=False, recursive=False, **kwargs):
        """Initialize the command.

        :param path: The path to be removed.
        :type path: str

        :param force: Force the removal.
        :type force: bool

        :param recursive: Remove all directories along the path.
        :type recursive: bool

        """
        self.force = force
        self.path = path
        self.recursive = recursive

        kwargs.setdefault("comment", "remove %s" % path)

        statement = ["rm"]

        if self.force:
            statement.append("-f")

        if self.recursive:
            statement.append("-r")

        statement.append(self.path)

        super().__init__(" ".join(statement), **kwargs)


class Rsync(Command):
    """Synchronize files from a local to remote directory."""

    def __init__(self, source, target, delete=False, exclude=None, guess=False, host=None, key_file=None, links=True,
                 port=22, recursive=True, user=None, **kwargs):
        """Initialize the command.

        :param source: The source directory.
        :type source: str

        :param target: The target directory.
        :type target: str

        :param delete: Indicates target files that exist in source but not in target should be removed.
        :type delete: bool

        :param exclude: The path to an exclude file.
        :type exclude: str

        :param guess: When ``True``, the ``host``, ``key_file``, and ``user`` will be guessed based on the base name of
                      the source path.
        :type guess: bool

        :param host: The host name or IP address. This causes the command to run over SSH and may require a
                     ``key_file``, ``port``, and ``user``.
        :type host: str

        :param key_file: The path to the private SSH key to use for remove connections. User expansion is
                         automatically applied.
        :type key_file: str

        :param links: Include symlinks in the sync.
        :type links: bool

        :param port: The SSH port to use for remote connections.
        :type port: int

        :param recursive: Indicates source contents should be recursively synchronized.
        :type recursive: bool

        :param user: The user name to use for remote connections.

        """
        self.delete = delete
        self.exclude_path = exclude
        self.links_enabled = links
        self.port = port
        self.recursive = recursive
        self.source = source
        self.target = target

        if guess:
            self.host = host or os.path.basename(source).replace("_", ".")
            self.key_file = key_file or os.path.expanduser(os.path.join("~/.ssh", os.path.basename(source)))
            self.user = user or os.path.basename(source)
        else:
            self.host = host
            self.key_file = key_file
            self.user = user

        kwargs.setdefault("comment", "copy %s to remote %s" % (source, target))

        # rsync -e "ssh -i $(SSH_KEY) -p $(SSH_PORT)" -P -rvzc --delete
        # $(OUTPUTH_PATH) $(SSH_USER)@$(SSH_HOST):$(UPLOAD_PATH) --cvs-exclude;

        tokens = list()
        tokens.append('rsync')
        tokens.append("--cvs-exclude")
        tokens.append("--checksum")
        tokens.append("--compress")

        if self.links_enabled:
            tokens.append("--copy-links")

        if self.delete:
            tokens.append("--delete")

        if self.exclude_path is not None:
            tokens.append("--exclude-from=%s" % self.exclude_path)

        # --partial and --progress
        tokens.append("-P")

        if self.recursive:
            tokens.append("--recursive")

        tokens.append(self.source)

        if all([self.host is not None, self.key_file is not None, self.user is not None]):
            tokens.append('-e "ssh -i %s -p %s"' % (self.key_file, self.port))
            tokens.append("%s %s@%s:%s" % (self.source, self.user, self.host, self.target))
        else:
            tokens.append("%s %s" % (self.source, self.target))

        statement = " ".join(tokens)

        super().__init__(statement, **kwargs)


class SCopy(Command):
    """Copy a file from the local (machine) to the remote host."""

    def __init__(self, from_path, to_path, host=None, key_file=None, port=22, user=None, **kwargs):
        """Initialize the command.

        :param from_path: The source directory.
        :type from_path: str

        :param to_path: The target directory.
        :type to_path: str

        :param host: The host name or IP address. Required.
        :type host: str

        :param key_file: The path to the private SSH key to use for remove connections. User expansion is
                         automatically applied.
        :type key_file: str

        :param port: The SSH port to use for remote connections.
        :type port: int

        :param user: The user name to use for remote connections.

        """
        self.from_path = from_path
        self.host = host
        self.key_file = key_file
        self.port = port
        self.to_path = to_path
        self.user = user

        kwargs.setdefault("comment", "copy %s to remote %s" % (from_path, to_path))

        # TODO: What to do to force local versus remote commands?
        # kwargs['local'] = True

        kwargs['sudo'] = False

        statement = ["scp"]

        if self.key_file is not None:
            statement.append("-i %s" % self.key_file)

        statement.append("-P %s" % self.port)
        statement.append(self.from_path)

        if self.host is not None and self.user is not None:
            statement.append("%s@%s:%s" % (self.user, self.host, self.to_path))
        elif self.host is not None:
            statement.append("%s:%s" % (self.host, self.to_path))
        else:
            raise ValueError("Host is a required keyword argument.")

        super().__init__(" ".join(statement), **kwargs)


class Sed(Command):
    """Replace text in a file."""

    def __init__(self, path, backup=".b", change=None, delimiter="/", find=None, **kwargs):
        """Initialize the command.

        :param path: The path to the file to be edited.
        :type path: str

        :param backup: The backup file extension to use.
        :type backup: str

        :param change: The new text. Required.
        :type change: str

        :param delimiter: The pattern delimiter.

        :param find: The old text. Required.
        :type find: str

        """
        self.backup = backup
        self.delimiter = delimiter
        self.find = find
        self.path = path
        self.replace = change

        kwargs.setdefault("comment", "find and replace in %s" % path)

        context = {
            'backup': self.backup,
            'delimiter': self.delimiter,
            'path': self.path,
            'pattern': self.find,
            'replace': self.replace,
        }

        template = "sed -i %(backup)s 's%(delimiter)s%(pattern)s%(delimiter)s%(replace)s%(delimiter)sg' %(path)s"

        statement = template % context

        super().__init__(statement, **kwargs)


class Symlink(Command):
    """Create a symlink."""

    def __init__(self, source, force=False, target=None, **kwargs):
        """Initialize the command.

        :param source: The source of the link.
        :type source: str

        :param force: Force the creation of the link.
        :type force: bool

        :param target: The name or path of the target. Defaults to the base name of the source path.
        :type target: str

        """
        self.force = force
        self.source = source
        self.target = target or os.path.basename(source)

        kwargs.setdefault("comment", "link to %s" % source)

        statement = ["ln -s"]

        if self.force:
            statement.append("-f")

        statement.append(self.source)
        statement.append(self.target)

        super().__init__(" ".join(statement), **kwargs)


class Touch(Command):
    """Touch a file or directory."""

    def __init__(self, path, **kwargs):
        """Initialize the command.

        :param path: The file or directory to touch.
        :type path: str

        """
        self.path = path

        kwargs.setdefault("comment", "touch %s" % path)

        super().__init__("touch %s" % self.path, **kwargs)


class Write(Command):
    """Write to a file."""

    def __init__(self, path, content=None, overwrite=False, **kwargs):
        """Initialize the command.

        :param path: The file to be written.
        :type from_path: str

        :param content: The content to be written. Note: If omitted, this command is equivalent to :py:class:`Touch`.
        :type content: str

        :param overwrite: Indicates file should be overwritten if it exists.
        :type overwrite: bool

        """
        self.path = path
        self.content = content or ""
        self.overwrite = overwrite

        kwargs.setdefault("comment", "write to %s" % path)

        a = list()

        self._operator = ">>"
        if overwrite:
            self._operator = ">"

        if len(self.content.split("\n")) > 1:
            a.append("cat %s %s << EOF" % (self._operator, path))
            a.append(self.content)
            a.append("EOF")
        else:
            a.append('echo "%s" %s %s' % (self.content, self._operator, path))

        super().__init__(" ".join(a), **kwargs)


MAPPING = {
    'append': Append,
    'copy': Copy,
    'cp': Copy,
    'link': Symlink,
    'makedir': MakeDir,
    'mkdir': MakeDir,
    'move': Move,
    'mv': Move,
    'perm': Permissions,
    'permissions': Permissions,
    'perms': Permissions,
    'remove': Remove,
    'rename': Move,
    'replace': Sed,
    'rm': Remove,
    'rsync': Rsync,
    'scp': SCopy,
    'sed': Sed,
    'sync': Rsync,
    'symlink': Symlink,
    'touch': Touch,
    'write': Write,
}
