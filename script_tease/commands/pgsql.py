# Imports

from .base import Command

# Exports

__all__ = (
    "MAPPING",
    "CreateDatabase",
    "CreateUser",
    "DatabaseExists",
    "DropDatabase",
    "DropUser",
    "DumpDatabase",
    "PSQL",
)

# Functions


def _get_pgsql_command(name, host="localhost", password=None, port=5432, user="postgres"):
    a = list()

    if password:
        a.append('export PGPASSWORD="%s" &&' % password)

    a.append(name)

    a.append("--host=%s" % host)
    a.append("--port=%s" % port)
    a.append("--username=%s" % user)

    return a

# Classes


class CreateDatabase(Command):
    """Create a PostgreSQL database."""

    def __init__(self, name, admin_pass=None, admin_user="postgres", host="localhost", owner=None, port=5432,
                 template=None, **kwargs):
        """Initialize the command.

        :param name: The database name.
        :type name: str

        :param admin_pass: The password for the user with sufficient access privileges to execute the command.
        :type admin_pass: str

        :param admin_user: The name of the user with sufficient access privileges to execute the command.
        :type admin_user: str

        :param host: The database host name or IP address.
        :type host: str

        :param owner: The owner (user/role name) of the new database.
        :type owner: str

        :param port: The port number of the Postgres service running on the host.
        :type port: int

        :param template: The database template name to use, if any.
        :type template: str

        """
        self.admin_pass = admin_pass
        self.admin_user = admin_user
        self.database = name
        self.host = host
        self.owner = owner or admin_user
        self.port = port
        self.template = template

        # Postgres commands always run without sudo because the -U may be provided.
        kwargs['sudo'] = False

        # Assemble the command.
        base = _get_pgsql_command("createdb", host=host, password=admin_pass, port=port)
        base.append("--owner=%s" % self.owner)

        if template is not None:
            base.append("--template=%s" % template)

        base.append(name)

        super().__init__(" ".join(base), **kwargs)


class CreateUser(Command):
    """Create a PostgreSQL user."""

    def __init__(self, name, admin_pass=None, admin_user="postgres", host="localhost", password=None, port=5432,
                 **kwargs):
        """Initialize the command.

        :param name: The user name.
        :type name: str

        :param admin_pass: The password for the user with sufficient access privileges to execute the command.
        :type admin_pass: str

        :param admin_user: The name of the user with sufficient access privileges to execute the command.
        :type admin_user: str

        :param host: The database host name or IP address.
        :type host: str

        :param password: The password for the new user.
        :type password: str

        :param port: The port number of the Postgres service running on the host.
        :type port: int

        """
        self.admin_pass = admin_pass
        self.admin_user = admin_user
        self.host = host
        self.password = password
        self.port = port
        self.user = name

        # Postgres commands always run without sudo because the -U may be provided.
        kwargs['sudo'] = False

        # Assemble the command.
        base = _get_pgsql_command("createuser", host=host, password=admin_pass, port=port)
        base.append("-DRS")
        base.append(name)

        if password is not None:
            base.append("&& psql -h %s -U %s" % (host, admin_user))
            base.append("-c \"ALTER USER %s WITH ENCRYPTED PASSWORD '%s';\"" % (name, password))

        super().__init__(" ".join(base), **kwargs)


class DatabaseExists(Command):
    """Determine if a Postgres database exists."""

    def __init__(self, name, admin_pass=None, admin_user="postgres", host="localhost", port=5432, **kwargs):
        """Initialize the command.

        :param name: The database name.
        :type name: str

        :param admin_pass: The password for the user with sufficient access privileges to execute the command.
        :type admin_pass: str

        :param admin_user: The name of the user with sufficient access privileges to execute the command.
        :type admin_user: str

        :param host: The database host name or IP address.
        :type host: str

        :param port: The port number of the Postgres service running on the host.
        :type port: int

        """
        self.admin_pass = admin_pass
        self.admin_user = admin_user
        self.database = name
        self.host = host
        self.port = port

        # Postgres commands always run without sudo because the -U may be provided. However, sudo may be required for
        # file writing.
        # kwargs['sudo'] = False

        kwargs.setdefault("register", "%s_db_exists" % name)

        base = _get_pgsql_command("psql", host=host, password=admin_pass, port=port, user=admin_user)
        base.append(r"-lqt | cut -d \| -f 1 | grep -qw %s" % name)

        super().__init__(" ".join(base), **kwargs)


class DropDatabase(Command):
    """Remove a PostgreSQL database."""

    def __init__(self, name, admin_pass=None, admin_user="postgres", host="localhost", port=5432, **kwargs):
        """Initialize the command.

        :param name: The database name.
        :type name: str

        :param admin_pass: The password for the user with sufficient access privileges to execute the command.
        :type admin_pass: str

        :param admin_user: The name of the user with sufficient access privileges to execute the command.
        :type admin_user: str

        :param host: The database host name or IP address.
        :type host: str

        :param port: The port number of the Postgres service running on the host.
        :type port: int

        """
        self.admin_pass = admin_pass
        self.admin_user = admin_user
        self.database = name
        self.host = host
        self.port = port

        # Postgres commands always run without sudo because the -U may be provided.
        kwargs['sudo'] = False

        # Assemble the command.
        base = _get_pgsql_command("dropdb", host=host, password=admin_pass, port=port)
        base.append(name)

        super().__init__(" ".join(base), **kwargs)


class DropUser(Command):
    """Remove a Postgres user."""

    def __init__(self, name, admin_pass=None, admin_user="postgres", host="localhost", port=5432, **kwargs):
        """Initialize the command.

        :param name: The user name.
        :type name: str

        :param admin_pass: The password for the user with sufficient access privileges to execute the command.
        :type admin_pass: str

        :param admin_user: The name of the user with sufficient access privileges to execute the command.
        :type admin_user: str

        :param host: The database host name or IP address.
        :type host: str

        :param port: The port number of the Postgres service running on the host.
        :type port: int

        """
        self.admin_pass = admin_pass
        self.admin_user = admin_user
        self.host = host
        self.port = port
        self.user = name

        # Postgres commands always run without sudo because the -U may be provided.
        kwargs['sudo'] = False

        # Assemble the command.
        base = _get_pgsql_command("dropuser", host=host, password=admin_pass, port=port)
        base.append(name)

        super().__init__(" ".join(base), **kwargs)


class DumpDatabase(Command):
    """Export a Postgres database."""

    def __init__(self, name, admin_pass=None, admin_user="postgres", file_name=None, host="localhost", port=5432,
                 **kwargs):
        """Initialize the command.

        :param name: The database name.
        :type name: str

        :param admin_pass: The password for the user with sufficient access privileges to execute the command.
        :type admin_pass: str

        :param admin_user: The name of the user with sufficient access privileges to execute the command.
        :type admin_user: str

        :param host: The database host name or IP address.
        :type host: str

        :param file_name: The name (including the path, if desired) of the export file. Defaults to the
                          ``database_name`` plus ".sql"
        :type file_name: str

        :param port: The port number of the Postgres service running on the host.
        :type port: int

        """
        self.admin_pass = admin_pass
        self.admin_user = admin_user
        self.database = name
        self.file_name = file_name or "%s.sql" % name
        self.host = host
        self.port = port

        # Postgres commands always run without sudo because the -U may be provided.
        kwargs['sudo'] = False

        # Assemble the command.
        base = _get_pgsql_command("pg_dump", host=host, password=admin_pass, port=port)
        base.append("--column-inserts")
        base.append("--file=%s" % self.file_name)
        base.append(name)

        super().__init__(" ".join(base), **kwargs)


class PSQL(Command):
    """Execute a psql command."""

    def __init__(self, sql, database="template1", host="localhost", password=None, port=5432, user="postgres",
                 **kwargs):
        """Initialize the command.

        :param sql: The SQL to be executed.
        :type sql: str

        :param database: The database name.
        :type database: str

        :param password: The password for the user with sufficient access privileges to execute the command.
        :type password: str

        :param host: The database host name or IP address.
        :type host: str

        :param port: The port number of the Postgres service running on the host.
        :type port: int

        :param user: The name of the user with sufficient access privileges to execute the command.
        :type user: str

        """
        self.database = database
        self.host = host
        self.password = password
        self.port = port
        self.sql = sql
        self.user = user

        # Postgres commands always run without sudo because the -U may be provided.
        kwargs['sudo'] = False

        # Assemble the command.
        base = _get_pgsql_command("psql", host=host, password=password, port=port)
        base.append("--dbname=%s" % database)
        base.append('-c "%s"' % sql)

        super().__init__(" ".join(base), **kwargs)


MAPPING = {
    'pg.client': PSQL,
    'pg.createdatabase': CreateDatabase,
    'pg.createdb': CreateDatabase,
    'pg.createuser': CreateUser,
    'pg.database': CreateDatabase,
    'pg.db': CreateDatabase,
    'pg.dropdatabase': DropDatabase,
    'pg.dropdb': DropDatabase,
    'pg.dropuser': DropUser,
    'pg.dump': DumpDatabase,
    'pg.dumpdb': DumpDatabase,
    'pg.exists': DatabaseExists,
    'pg.user': CreateUser,
    'psql': PSQL,
}
