# Imports

from .base import Command

# Exports

__all__ = (
    "MAPPING",
    "UDF",
)

# Constants

UDF_DOCS = """
This produces output similar to the code below.

.. code-block:: bash

    # <UDF name="deploy_user" label="Deployment User" example="example_app" />
    # <UDF name="deploy_root" label="Deployment Root" example="/opt/example_app" />
    # <UDF name="deploy_key" label="Deploy Key" example="paste public key here" />
    # <UDF name="domain_name" label="Domain Name" example="example.com" />
    # <UDF name="postgres_version" label="Postgres Version" default="10" />

"""

# Classes


class UDF(Command):
    """Create a UDF input for a Linode StackScript."""

    def __init__(self, name, default=None, example=None, label=None, **kwargs):
        self.default = default
        self.example = example
        self.label = label or name.replace("_", " ").title()
        self.name = name

        a = ['# <UDF name="%s" label="%s"' % (self.name, self.label)]

        if self.default is not None:
            a.append('default="%s"' % self.default)
        elif self.example is not None:
            a.append('example="%s"' % self.example)
        else:
            pass

        a.append("/>")

        statement = " ".join(a)

        super().__init__(statement, **kwargs)

    @classmethod
    def get_docs(cls):
        """Provide additional information."""

    def get_statement(self, cwd=False):
        """Override to return only the statement without any additional options."""
        return self.statement


MAPPING = {
    'udf': UDF,
    'stackscript.udf': UDF,
}
