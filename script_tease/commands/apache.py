# Imports

import logging
from ..constants import LOGGER_NAME
from .base import Command

log = logging.getLogger(LOGGER_NAME)

# Exports

__all__ = (
    "MAPPING",
    # "Apache",
    "ConfigTest",
    "Disable",
    "DisableModule",
    "DisableSite",
    "Enable",
    "EnableModule",
    "EnableSite",
)

# Classes


# class Apache(Command):
#
#     def __init__(self, *args, **kwargs):
#         self._args = args
#         self._kwargs = kwargs
#         super().__init__("", **kwargs)
#
#     def get_statement(self, cwd=False):
#         i = 0
#         for arg in self._args:
#             if arg in ("config", "configtest"):
#                 return ConfigTest(True, **self._kwargs).get_statement(cwd=cwd)
#             elif arg == "disable":
#                 try:
#                     name = self._args[i + 1]
#                     target = self._args[i + 2]
#                 except IndexError:
#                     log.warning("Invalid specification for apache disable.")
#                     return None
#
#                 if name in ("mod", "module"):
#                     return DisableModule(target, **self._kwargs).get_statement(cwd=cwd)
#                 elif name == "site":
#                     return DisableSite(target, **self._kwargs).get_statement(cwd=cwd)
#                 else:
#                     log.warning("Invalid option for apache disable: %s" % name)
#                     return None
#             elif arg == "enable":
#                 try:
#                     name = self._args[i + 1]
#                     target = self._args[i + 2]
#                 except IndexError:
#                     log.warning("Invalid specification for apache enable.")
#                     return None
#
#                 if name in ("mod", "module"):
#                     return EnableModule(target, **self._kwargs).get_statement(cwd=cwd)
#                 elif name == "site":
#                     return EnableSite(target, **self._kwargs).get_statement(cwd=cwd)
#                 else:
#                     log.warning("Invalid option for apache enable: %s" % name)
#                     return None
#             else:
#                 log.warning("Invalid option for apache: %s" % arg)
#
#             i += 1


class ConfigTest(Command):
    """Run an apache config test."""

    def __init__(self, **kwargs):
        """There is no argument."""
        statement = "apachectl configtest"

        kwargs.setdefault('register', "apache_checks_out")

        super().__init__(statement, **kwargs)


class Disable(Command):

    def __init__(self, what, name, **kwargs):
        if what in ("mod", "module"):
            statement = DisableModule(name, **kwargs).statement
        elif what == "site":
            statement = DisableSite(name, **kwargs).statement
        else:
            raise ValueError("Invalid Apache item to be disabled: %s" % what)

        super().__init__(statement, **kwargs)


class DisableModule(Command):
    """Disable an Apache module."""

    def __init__(self, module_name, **kwargs):
        """Initialize the command.

        :param module_name: The module name.
        :type module_name: str

        """
        statement = "a2dismod %s" % module_name

        super().__init__(statement, **kwargs)


class DisableSite(Command):
    """Disable a virtual host."""

    def __init__(self, domain_name, **kwargs):
        """Initialize the command.

        :param domain_name: The domain name.
        :type domain_name: str

        """
        statement = "a2dissite %s.conf" % domain_name

        super().__init__(statement, **kwargs)


class Enable(Command):

    def __init__(self, what, name, **kwargs):
        if what in ("mod", "module"):
            statement = EnableModule(name, **kwargs).statement
        elif what == "site":
            statement = EnableSite(name, **kwargs).statement
        else:
            raise ValueError("Invalid Apache item to be enabled: %s" % what)

        super().__init__(statement, **kwargs)


class EnableModule(Command):
    """Enable an Apache module."""

    def __init__(self, module_name, **kwargs):
        """Initialize the command.

        :param module_name: The module name.
        :type module_name: str

        """
        statement = "a2enmod %s" % module_name

        super().__init__(statement, **kwargs)


class EnableSite(Command):
    """Enable a virtual host."""

    def __init__(self, domain_name, **kwargs):
        """Initialize the command.

        :param domain_name: The domain name.
        :type domain_name: str

        """
        statement = "a2ensite %s.conf" % domain_name

        super().__init__(statement, **kwargs)


MAPPING = {
    # 'apache': Apache,
    'apache.check': ConfigTest,
    'apache.config': ConfigTest,
    'apache.configtest': ConfigTest,
    'apache.disable': Disable,
    'apache.disable_mod': DisableModule,
    'apache.disable module': DisableModule,
    'apache.disable_site': DisableSite,
    'apache.enable': Enable,
    'apache.enable_mod': EnableModule,
    'apache.enable_module': EnableModule,
    'apache.enable_site': EnableSite,
    'apache.mod': EnableModule,
    'apache.module': EnableModule,
    'apache.test': ConfigTest,
}
