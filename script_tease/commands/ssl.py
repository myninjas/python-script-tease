# Imports

import os
from .base import Command

# Exports

__all__ = (
    "MAPPING",
    "Certbot",
)

# Classes


class Certbot(Command):
    """Get new SSL certificate from Let's Encrypt."""

    def __init__(self, domain_name, email=None, webroot=None, **kwargs):
        """Initialize the command.

        :param domain_name: The domain name for which the SSL certificate is requested.
        :type domain_name: str

        :param email: The email address of the requester sent to the certificate authority. Required.
        :type email: str

        :param webroot: The directory where the challenge file will be created.
        :type webroot: str

        """
        self.domain_name = domain_name
        self.email = email
        self.webroot = webroot or os.path.join("/var", "www", "domains", domain_name.replace(".", "_"), "www")

        if not email:
            raise ValueError("Email is required for certbot command.")

        template = "certbot certonly --agree-tos --email %(email)s -n --webroot -w %(webroot)s -d %(domain_name)s"
        name = template % {
            'domain_name': domain_name,
            'email': email,
            'webroot': self.webroot,
        }

        super().__init__(name, **kwargs)


MAPPING = {
    'certbot': Certbot,
    'ssl': Certbot,
}
