# Imports

import os

# Exports

__all__ = (
    "MAPPING",
    "Command",
    "Include",
    "ItemizedCommand",
    "Sudo",
)

# Classes


class Command(object):
    """A command to be executed."""

    def __init__(self, statement, comment=None, condition=None, cwd=None, environments=None, function=None, prefix=None,
                 register=None, shell=None, stop=False, sudo=None, tags=None, **kwargs):
        """Initialize a command.

        :param statement: The statement to be executed.
        :type statement: str

        :param comment: A comment regarding the command.
        :type comment: str

        :param condition: A condition for execution. For example, ``! -f /path/to/some/file.txt``
        :type condition: str

        :param cwd: The statement should be executed from this directory.
        :type cwd: str

        :param environments: A list of environment names in which the statement is executed.
        :type environments: list[str]

        :param function: The function name, if any, into which this command is placed.
        :type function: str

        :param prefix: An additional statement to be executed before the command.
        :type prefix: str

        :param register: A variable name to which the the success or failure of the statement is captured.
        :type register: str

        :param shell: Force the given shell to be used when executing the statement.
        :type shell: str

        :param stop: Indicates processing should stop if this statement fails.
        :type stop: bool

        :param sudo: Indicates whether sudo should be used. May be given as a ``bool`` or user name.
        :type sudo: bool | str

        :param tags: A list of tags describing the command.
        :type tags: list[str]

        .. note:
            Additional keyword arguments are not used by Script Tease but are available via the getter.

        """
        self.comment = comment
        self.condition = condition
        self.cwd = cwd
        self.environments = environments or list()
        self.function = function
        self.prefix = prefix
        self.register = register
        self.shell = shell
        self.statement = statement
        self.stop = stop
        self.tags = tags or list()

        if isinstance(sudo, Sudo):
            self.sudo = Sudo
        elif type(sudo) is str:
            self.sudo = Sudo(enabled=True, user=sudo)
        elif sudo is True:
            self.sudo = Sudo(enabled=True)
        else:
            self.sudo = Sudo()

        self._attributes = kwargs

    def __getattr__(self, item):
        return self._attributes.get(item)

    def __repr__(self):
        return "<%s>" % self.__class__.__name__

    @classmethod
    def get_docs(cls):
        """Get documentation for the command.

        :rtype: str | None

        """
        return None

    def get_statement(self, cwd=False):
        """Get the full statement.

        :param cwd: Include the directory change, if given.
        :type cwd: bool

        :rtype: str

        """
        a = list()

        if cwd and self.cwd is not None:
            a.append("( cd %s &&" % self.cwd)

        if self.prefix is not None:
            a.append("%s &&" % self.prefix)

        if self.sudo:
            statement = "sudo -u %s %s" % (self.sudo.user, self._get_statement())
        else:
            statement = self._get_statement()

        a.append("%s" % statement)

        if cwd and self.cwd is not None:
            a.append(")")

        b = list()
        if self.comment is not None:
            b.append("# %s" % self.comment)

        if self.condition is not None:
            b.append("if [[ %s ]]; then %s; fi;" % (self.condition, " ".join(a)))
        else:
            b.append(" ".join(a))

        if self.register is not None:
            b.append("%s=$?;" % self.register)

            if self.stop:
                b.append("if [[ $%s -gt 0 ]]; exit 1; fi;" % self.register)
        elif self.stop:
            b.append("if [[ $? -gt 0 ]]; exit 1; fi;")
        else:
            pass

        # if self.condition is not None:
        #     b.append("fi;")
        #     b.append("")

        return "\n".join(b)

    def preview(self, cwd=True):
        """Preview the statement to be executed.

        :param cwd: Include the directory change, if given.
        :type cwd: bool

        :rtype: str

        """
        return self.get_statement(cwd=cwd)

    def _get_statement(self):
        """By default, get the statement passed upon command initialization.

        :rtype: str

        """
        return self.statement


class Include(object):
    """Include commands from another file."""

    def __init__(self, path, comment=None, locations=None):
        """Initialize an include.

        :param path: The path to the configuration file whose commands should be included.
        :type path: str

        :param comment: The comment on the include.
        :type comment: str

        :param locations: The locations where the included file may be found.
        :type locations: list[str]
        """
        self.comment = comment
        self.locations = locations or list()
        self.path = path

    @classmethod
    def get_docs(cls):
        """Get documentation for the command.

        :rtype: str | None

        """
        return None

    def get_path(self):
        """Get the actual path of the include.

        :rtype: str | None

        1. If the path exists as is, it is returned first.
        2. The path is checked with each possible location, and is returned if a valid path is found.
        3. ``None`` is returned if not path is found.

        """
        if os.path.exists(self.path):
            return self.path

        for location in self.locations:
            path = os.path.join(location, self.path)
            if os.path.exists(path):
                return path

        return None

    def load(self, config_class, **kwargs):
        """Load the commands from the included configuration file.

        :param config_class: The class to use for parsing the configuration file.
        :type config_class: class

        :param kwargs: Passed when to the ``config_class`` upon instantiation.

        :rtype: list[BaseType[Command]] | None
        :returns: If the path is found and the configuration successfully loaded, the commands are returned. Otherwise,
                  ``None``.

        """
        path = self.get_path()
        if path is None:
            return None

        config = config_class(path, **kwargs)
        if config.load():
            return config.get_commands()

        return None


class ItemizedCommand(object):
    """Execute the same command multiple times with different arguments."""

    def __init__(self, command_class, items, *args, **kwargs):
        """Initialize the command.

        :param command_class: The command class to be used.
        :type command_class: class

        :param items: The command arguments.
        :type items: list[str]

        :param args: The itemized arguments. ``$item`` should be included.

        :param kwargs: Keyword arguments are passed to the command class upon instantiation.

        """
        self.args = args
        self.command_class = command_class
        self.items = items
        self.kwargs = kwargs

    def __getattr__(self, item):
        return self.kwargs.get(item)

    def __repr__(self):
        return "<%s %s>" % (self.__class__.__name__, self.command_class.__name__)

    def get_commands(self):
        """Get the commands to be executed.

        :rtype: list[BaseType(Command)]

        """
        kwargs = self.kwargs.copy()

        a = list()
        for item in self.items:
            args = list()
            for arg in self.args:
                args.append(arg.replace("$item", item))

            command = self.command_class(*args, **kwargs)
            a.append(command)

        return a

    @classmethod
    def get_docs(cls):
        """Get documentation for the command.

        :rtype: str | None

        """
        return None

    def get_statement(self, cwd=False):
        """Override to get multiple commands."""
        kwargs = self.kwargs.copy()
        comment = kwargs.pop("comment", "execute multiple commands")

        a = list()
        # a.append("# %s" % comment)

        commands = self.get_commands()
        for c in commands:
            a.append(c.preview(cwd=cwd))
            a.append("")

        # for item in self.items:
        #     args = list()
        #     for arg in self.args:
        #         args.append(arg.replace("$item", item))
        #
        #     command = self.command_class(*args, **kwargs)
        #     a.append(command.preview(cwd=cwd))
        #     a.append("")

        return "\n".join(a)

    def preview(self, cwd=True):
        """Preview the statement to be executed.

        :param cwd: Include the directory change, if given.
        :type cwd: bool

        :rtype: str

        """
        return self.get_statement(cwd=cwd)


class Sudo(object):
    """Helper class for defining sudo options."""

    def __init__(self, enabled=False, user="root"):
        """Initialize the helper.

        :param enabled: Indicates sudo is enabled.
        :type enabled: bool

        :param user: The user to be invoked.
        :type user: str

        """
        self.enabled = enabled
        self.user = user

    def __bool__(self):
        return self.enabled


MAPPING = {
    'cmd': Command,
    'command': Command,
    'do': Command,
    'include': Include,
    'run': Command,
}
