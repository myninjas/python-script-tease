# Imports

from .base import Command

# Exports

__all__ = (
    "MAPPING",
    "Apt",
    "Pip",
    "VirtualEnv",
    "Yum",
)

# Classes


class Apt(Command):
    """Install a package using apt-get."""

    def __init__(self, package, remove=False, **kwargs):
        """Initialize the command.

        :param package: The package to be installed (or removed).
        :type package: str

        :param remove: Remove, rather than install, the package.
        :type remove: bool

        """
        self.package = package

        if remove:
            kwargs.setdefault("comment", "uninstall %s" % package)
            statement = "apt-get uninstall -y %s" % self.package
        else:
            kwargs.setdefault("comment", "install %s" % package)
            statement = "apt-get install -y %s" % self.package

        super().__init__(statement, **kwargs)


class Pip(Command):
    """Install a Python package using pip."""

    def __init__(self, package, remove=False, upgrade=False, **kwargs):
        """Initialize the command.

        :param package: The package to be installed (or removed).
        :type package: str

        :param remove: Remove, rather than install, the package.
        :type remove: bool

        :param upgrade: Upgrade the package.
        :type upgrade: bool

        """
        self.package = package

        if remove:
            kwargs.setdefault("comment", "uninstall %s" % package)
            statement = "pip3 uninstall --quite %s" % self.package
        else:
            kwargs.setdefault("comment", "install %s" % package)

            a = list()
            a.append("pip3 install --quiet")

            if upgrade:
                a.append("--upgrade")

            a.append(self.package)

            statement = " ".join(a)

        super().__init__(statement, **kwargs)


class VirtualEnv(Command):
    """Create a Python virtual environment."""

    def __init__(self, name, **kwargs):
        """Initialize a Python virtual environment.

        :param name: The name of the environment.
        :type name: str

        """
        self.environment_name = name

        kwargs.setdefault("comment", "create %s virtual environment" % name)

        statement = "virtualenv %s" % self.environment_name

        super().__init__(statement, **kwargs)


class Yum(Command):
    """Install a package using yum."""

    # http://man7.org/linux/man-pages/man8/yum.8.html

    def __init__(self, package, remove=False, **kwargs):
        """Initialize the command.

        :param package: The package to be installed (or removed).
        :type package: str

        :param remove: Remove, rather than install, the package.
        :type remove: bool

        """
        self.package = package

        if remove:
            kwargs.setdefault("comment", "uninstall %s" % package)
            statement = "yum -y uninstall %s" % self.package
        else:
            kwargs.setdefault("comment", "install %s" % package)
            statement = "yum -y install %s" % self.package

        super().__init__(statement, **kwargs)


MAPPING = {
    'apt': Apt,
    'pip': Pip,
    'venv': VirtualEnv,
    'virtualenv': VirtualEnv,
    'yum': Yum,
}
