"""
Users
.....

Classes for working with user accounts.

"""
# Imports

from .base import Command

# Exports

__all__ = (
    "MAPPING",
    "AddUser",
)

# Classes


class AddUser(Command):
    """Add a user account."""

    def __init__(self, name, groups=None, home=None, **kwargs):
        """Initialize the command.

        :param name: The name of the user to be created.
        :type name: str

        :param groups: The groups to which the user belongs. Maybe given as a list, tuple, or comma separated string.
        :type groups: list[str] | str

        :param home: Override's the user's home directory.
        :type home: str

        """
        self.groups = groups
        self.home = home
        self.user_name = name

        kwargs.setdefault("comment", "create user %s" % name)

        super().__init__("", **kwargs)

        commands = list()

        # The gecos switch eliminates the prompts.
        statement = 'adduser %s --disable-password --gecos ""' % name

        if home is not None:
            statement += " --home %s" % home

        commands.append(Command(statement, **kwargs))

        if groups is not None:
            if type(groups) is str:
                _groups = list()
                for i in groups.split(","):
                    _groups.append(i.strip())
            elif type(groups) in (list, tuple):
                _groups = groups
            else:
                raise TypeError("groups parameter must be a comma separated string, list, or tuple")

            for group in _groups:
                commands.append(Command("adduser %s %s" % (name, group), **kwargs))

            if "sudo" in _groups:
                commands.append(Command(
                    'echo "%s ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/%s' % (name, name),
                    **kwargs
                ))

        self._commands = commands

    def get_statement(self, cwd=False):
        a = list()
        a.append("# %s" % self.comment)

        for c in self._commands:
            c.comment = None
            a.append(c.get_statement(cwd=cwd))

        return "\n".join(a)


MAPPING = {
    'adduser': AddUser,
}
