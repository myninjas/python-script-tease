# Imports

from .base import Command

# Exports

__all__ = (
    "MAPPING",
    "Message",
    "Slack",
)

# Constants

SLACK_DOCS = """
This uses the Incoming Webhooks feature, which requires some additional setup.

.. note::
    The following steps were accurate as of April 2019.

**1.** Log in to Slack and go to `Your Apps`_.

.. _Your Apps: https://api.slack.com/apps

**2.** Create a new Slack app.

**3.** On the next page, select Incoming Webhooks and then toggle activation.

.. image:: /_static/images/slack-1.jpg

**4.** Next click Add new Webhook to Workspace and select the channel to which the message will be posted.

.. image:: /_static/images/slack-2.jpg

.. image:: /_static/images/slack-3.jpg

**5.** Copy the URL for the new webhook to use as the ``url`` parameter for the Slack command.
    
"""

# Classes


class Message(Command):

    def __init__(self, output, back_title="Message", dialog=False, height=15, width=100, **kwargs):
        """Send a message to the user.

        :param output: The message to be sent.
        :type output: str

        :param back_title: The back title of the input. Used only when ``dialog`` is enabled.
        :type back_title: str

        :param dialog: Indicates the dialog command should be used.
        :type dialog: bool

        :param height: The height of the message when using ``dialog``.
        :type height: int

        :param width: The width of the message when using ``dialog``.
        :type width: int

        """
        self.back_title = back_title
        self.dialog_enabled = dialog
        self.height = height
        self.output = output
        self.width = width

        if self.dialog_enabled:
            a = list()
            a.append("dialog --clear")
            if self.back_title is not None:
                a.append('--backtitle "%s"' % self.back_title)

            a.append('--msgbox "%s"' % self.output)
            a.append("%s %s" % (self.height, self.width))
            a.append("; clear")

            statement = " ".join(a)
        else:
            statement = 'echo "%s"' % output

        super().__init__(statement, **kwargs)


class Slack(Command):
    """Send a message to Slack."""

    def __init__(self, message, url=None, **kwargs):
        """Initialize the command.

        :param message: The message to be sent.
        :type message: str

        :param url: The URL of the webhook. Required.
        :type url: str

        .. note::
            Common command parameters are currently ignored.

        """

        if url is None:
            raise ValueError("The url parameter is required for Slack messages.")

        self.message = message
        self.url = url

        super().__init__("slack", **kwargs)

    @classmethod
    def get_docs(cls):
        """Provide additional instructions."""
        return SLACK_DOCS

    def get_statement(self, cwd=False):
        """Override to return a curl command string."""
        a = list()

        a.append("curl -X POST -H 'Content-type: application/json' --data")
        a.append("'" + '{"text": "%s"}' % self.message + "'")
        a.append(self.url)

        return " ".join(a)


MAPPING = {
    'echo': Message,
    'feedback': Message,
    'message': Message,
    'msg': Message,
    'slack': Slack,
}

