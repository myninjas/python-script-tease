# Imports

import os
from .base import Command

# Exports

__all__ = (
    "MAPPING",
    "Check",
    "CollectStatic",
    "Django",
    "DumpData",
    "LoadData",
    "Migrate",
)

# Classes


class Django(Command):
    """Run a Django management command."""

    def __init__(self, name, *args, **kwargs):
        self._args = args
        self._name = name

        # Base parameters need to be captured, because all others are assumed to be switches for the management command.
        _kwargs = {
            'comment': kwargs.pop("comment", None),
            'condition': kwargs.pop("condition", None),
            'cwd': kwargs.pop("cwd", None),
            'environments': kwargs.pop("environments", None),
            'function': kwargs.pop("function", None),
            # 'local': kwargs.pop("local", False),
            'prefix': kwargs.pop("prefix", None),
            'register': kwargs.pop("register", None),
            'shell': kwargs.pop("shell", "/bin/bash"),
            'stop': kwargs.pop("stop", False),
            'sudo': kwargs.pop('sudo', False),
            'tags': kwargs.pop("tags", None),
        }
        self._kwargs = _kwargs
        self._switches = kwargs

        statement = "./manage.py %s" % name
        super().__init__(statement, **_kwargs)

    def _get_statement(self):

        a = list()
        a.append("./manage.py %s" % self._name)

        for key, value in self._switches.items():
            key = key.replace("_", "-")
            if type(value) is bool:
                a.append("--%s" % key)
            else:
                a.append("--%s=%s" % (key, value))

        if self._args:
           a.append(" ".join(self._args))

        return " ".join(a)


class Check(Django):
    """Run Django checks."""

    def __init__(self, **kwargs):
        kwargs.setdefault("register", "django_checks_out")

        super().__init__("check", **kwargs)


class CollectStatic(Django):
    """Collect static files."""

    def __init__(self, **kwargs):
        kwargs.setdefault("noinput", True)

        super().__init__("collectstatic", **kwargs)


class DumpData(Django):
    """Export Django fixtures."""

    def __init__(self, app_name, base_path="local", file_name="initial", indent=4, natural_foreign=False, natural_primary=False, path=None,
                 **kwargs):
        """Initialize the command.

        :param app_name: The name (app label) of the app. ``app_label.ModelName`` may also be given.
        :type app_name: str

        :param file_name: The file name to which the data will be dumped.
        :type file_name: str

        :param indent: Indentation of the exported fixtures.
        :type indent: int

        :param natural_foreign: Use the natural foreign parameter.
        :type natural_foreign: bool

        :param natural_primary: Use the natural primary parameter.
        :type natural_primary: bool

        :param path: The path to the data file.
        :type path: str

        """
        self.app_name = app_name
        self.base_path = base_path
        self.file_name = file_name
        self.indent = indent
        self.natural_foreign = natural_foreign
        self.natural_primary = natural_primary
        self.output_format = kwargs.pop("format", "json")
        self.path = path

        super().__init__("dumpdata", **kwargs)

    def get_path(self):
        """Get the full path of the data file."""
        if self.path is not None:
            return self.path

        return os.path.join(self.base_path, self.app_name, "fixtures", "%s.%s" % (self.file_name, self.output_format))

    def _get_statement(self):
        a = list()
        a.append("./manage.py dumpdata")
        a.append("--format=%s" % self.output_format)
        a.append("--indent=%s" % self.indent)

        if self.natural_foreign:
            a.append("--natural-foreign")

        if self.natural_primary:
            a.append("--natural-primary")

        a.append(self.app_name)

        a.append("> %s" % self.get_path())

        return " ".join(a)


class LoadData(Django):
    """Load Django fixtures."""

    def __init__(self, app_name, base_path="local", file_name="initial", path=None, **kwargs):
        """Initialize the command.

        :param app_name: The name (app label) of the app.
        :type app_name: str

        :param file_name: The file name to which the data will be dumped.
        :type file_name: str

        :param natural_primary: Use the natural primary parameter.
        :type natural_primary: bool

        :param path: The path to the data file.
        :type path: str

        """
        self.app_name = app_name
        self.base_path = base_path
        self.file_name = file_name
        self.input_format = kwargs.pop("format", "json")
        self.path = path

        super().__init__("loaddata", **kwargs)

    def get_path(self):
        """Get the full path of the data file."""
        if self.path is not None:
            return self.path

        return os.path.join(self.base_path, self.app_name, "fixtures", "%s.%s" % (self.file_name, self.input_format))

    def _get_statement(self):
        """Override to handle file path."""
        return "./manage.py loaddata %s" % self.get_path()


class Migrate(Django):
    """Apply Django database migrations."""

    def __init__(self, **kwargs):
        super().__init__("migrate", **kwargs)


MAPPING = {
    'django': Django,
    'django.check': Check,
    'django.collectstatic': CollectStatic,
    'django.dd': DumpData,
    'django.dump': DumpData,
    'django.dumpdata': DumpData,
    'django.ld': LoadData,
    'django.load': LoadData,
    'django.loaddata': LoadData,
    'django.migrate': Migrate,
    'django.static': CollectStatic,
}

SUB_COMMANDS = {
    'check': Check,
    'collectstatic': CollectStatic,
    'dd': DumpData,
    'dump': DumpData,
    'dumpdata': DumpData,
    'ld': LoadData,
    'load': LoadData,
    'loaddata': LoadData,
    'migrate': Migrate,
    'static': CollectStatic,
}
