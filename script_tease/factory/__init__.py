# Imports

import logging
from ..commands import ItemizedCommand
from ..mappings import MAPPING, command_exists
from ..scripts import Function

log = logging.getLogger("script-tease")

# Exports

__all__ = (
    "factory",
)

# Constants

DOTTED_COMMANDS = (
    "apache",
    "django",
    "pg",
)

# Functions


def factory(name, comment, *args, **kwargs):
    """Load a named command.

    :param name: The command name.
    :type name: str

    :param comment: The comment on the command.
    :type comment: str

    ``args`` and ``kwargs`` are passed to the command upon instantiation.

    :rtype: BaseType[Command] | ItemizedCommand

    .. note::
        All exceptions are handled, but errors are logged.

    """
    if name in ("func", "function"):
        kwargs['comment'] = comment
        return Function(*args, **kwargs)

    _args = list(args)

    if name in DOTTED_COMMANDS:
        try:
            sub_command = _args.pop(0)
            _name = "%s.%s" % (name, sub_command)

            # If the dotted command exists, override the name. Otherwise let the target module attempt to handle a
            # custom command name.
            if command_exists(_name):
               name = _name
            else:
                _args.insert(0, sub_command)

        except IndexError:
            pass

    if not command_exists(name):
        log.warning("No mapping for command: %s" % name)
        return None

    kwargs['comment'] = comment

    log.debug("%s: %s" % (comment, kwargs))

    command_class = MAPPING[name]
    try:
        items = kwargs.pop("items", None)
        if items is not None:
            return ItemizedCommand(command_class, items, *_args, **kwargs)

        return command_class(*_args, **kwargs)
    except (KeyError, TypeError, ValueError) as e:
        log.critical("Failed to load %s command: %s" % (name, e))
        return None
