# Imports

import logging
from myninjas.utils import File
from ..constants import LOGGER_NAME
from ..utils import filter_objects
from .config import Config

log = logging.getLogger(LOGGER_NAME)

# Exports

__all__ = (
    "load_commands",
    "load_config",
)

# Functions


def load_commands(path, filters=None, **kwargs):
    input_file = File(path)
    if input_file.extension == ".ini":
        _config = Config(input_file.path, **kwargs)
    else:
        log.warning("Input file format is not currently supported: %s" % input_file.extension)
        return None

    if _config.load():
        commands = _config.get_commands()

        if filters is not None:
            criteria = dict()
            for attribute, values in filters.items():
                criteria[attribute] = values

            commands = filter_objects(commands, **criteria)

        return commands

    log.error("Failed to load config file: %s" % input_file.path)
    return None


def load_config(path, **kwargs):
    input_file = File(path)
    if input_file.extension == ".ini":
        _config = Config(input_file.path, **kwargs)
    else:
        log.warning("Input file format is not currently supported: %s" % input_file.extension)
        return None

    if not _config.load():
        log.error("Failed to load config file: %s" % input_file.path)
        return None

    return _config
