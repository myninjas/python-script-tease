# Imports

from configparser import ConfigParser, ParsingError
import logging
from myninjas.utils import parse_jinja_template, smart_cast, File
import os
from ..commands import Include, ItemizedCommand
from ..commands.templates import Template
from ..factory import factory
from ..scripts import Function, Script

log = logging.getLogger("script-tease")

# Exports

__all__ = (
    "Config",
)

# Classes


class Config(File):
    """Load commands from an INI file."""

    def __init__(self, path, context=None, locations=None, options=None):
        """Initialize the  configuration.

        :param path: The path to the INI file.
        :type path: str

        :param context: If given, the INI file will be parsed as a Jinja2 template prior to loading the commands.
        :type context: dict

        :param locations: A list of paths where script-related files may be found (such as templates).
        :type locations: list[str]

        :param options: Common options given to every command instantiated by the configuration. These provide the basis
                        of command keyword arguments, but if the same keyword is specified in the file, the option is
                        overridden.
        :type options: dict

        """
        super().__init__(path)

        self.context = context
        self.is_loaded = False
        self.locations = locations or list()
        self.options = options or dict()
        self._commands = list()
        self._functions = list()

    def get_commands(self):
        """Get the commands that have been loaded from the file.

        :rtype: list[BaseType[Command] | ItemizedCommand]

        """
        a = list()
        for c in self._commands:
            if c.function is not None:
                continue

            a.append(c)

        return a

    def get_functions(self):
        """Get the functions that have been loaded from the file.

        :rtype: list[Function]

        """
        a = list()
        for f in self._functions:
            for c in self._commands:
                if c.function is not None and f.name == c.function:
                    f.commands.append(c)

            a.append(f)

        return a

    def load(self):
        """Load the file.

        :rtype: bool

        """
        if not self.exists:
            return False

        ini = self._load_ini()
        if ini is None:
            return False

        success = True
        for section in ini.sections():
            args = list()
            command_name = None
            count = 0
            kwargs = self.options.copy()

            for key, value in ini.items(section):
                # First set is the command name and arguments.
                if count == 0:
                    command_name = key

                    if value[0] == '"':
                        args.append(value.replace('"', ""))
                    else:
                        args = value.split(" ")
                elif key in ("environments", "environs", "envs", "env", "items", "tags"):
                    if key in ("environments", "environs", "envs", "env"):
                        key = "environments"

                    items = list()
                    for i in value.split(","):
                        items.append(i.strip())

                    kwargs[key] = items
                elif key in ("func", "function"):
                    kwargs['function'] = value
                else:
                    kwargs[key] = smart_cast(value)

                count += 1

            command = factory(command_name, section, *args, **kwargs)
            if command is not None:

                if isinstance(command, Function):
                    self._functions.append(command)
                elif isinstance(command, Include):
                    subcommands = self._load_include(command)
                    if subcommands is not None:
                        self._commands += subcommands
                elif isinstance(command, Template):
                    self._load_template(command)
                    self._commands.append(command)
                elif isinstance(command, ItemizedCommand) and issubclass(command.command_class, Template):
                    for c in command.get_commands():
                        self._load_template(c)
                        self._commands.append(c)
                else:
                    self._commands.append(command)
            else:
                success = False

        self.is_loaded = success
        return self.is_loaded

    def _load_include(self, command):
        """Load an include command.

        :param command: The include command.
        :type command: Include

        :rtype: list | None

        """
        command.locations += self.locations
        command.locations.append(os.path.join(self.directory, "templates"))
        command.locations.append(self.directory)

        return command.load(
            self.__class__,
            context=self.context,
            locations=self.locations,
            options=self.options
        )

    def _load_ini(self):
        """Load the configuration file.

        :rtype: ConfigParser | None

        """
        ini = ConfigParser()
        if self.context is not None:
            try:
                content = parse_jinja_template(self.path, self.context)
            except Exception as e:
                log.error("Failed to parse %s as template: %s" % (self.path, e))
                return None

            try:
                ini.read_string(content)
                return ini
            except ParsingError as e:
                log.error("Failed to parse %s: %s" % (self.path, e))
        else:
            try:
                ini.read(self.path)
                return ini
            except ParsingError as e:
                log.error("Failed to parse %s: %s" % (self.path, e))
                return None

    def _load_template(self, command):
        """Load additional resources for a template command.

        :param command: The template command.
        :type command: Template

        """
        # This may produce problems if template kwargs are the same as the given context.
        if self.context is not None:
            command.context.update(self.context)

        # Custom locations come before default locations.
        command.locations += self.locations

        # This allows template files to be specified relative to the configuration file.
        command.locations.append(os.path.join(self.directory, "templates"))
        command.locations.append(self.directory)

    def to_script(self):
        """Convert loaded commands to a script.

        :rtype: Script

        """
        return Script(
            "%s.sh" % self.name,
            commands=self.get_commands(),
            functions=self.get_functions()
        )
