# Imports

from configparser import ConfigParser
import logging
from myninjas.utils import smart_cast
import os
from ..constants import LOGGER_NAME
from ..utils import split_csv

log = logging.getLogger(LOGGER_NAME)

# Exports

__all__ = (
    "Context",
    "Variable",
)

# Functions


def load_variables(path, environment=None):
    if not os.path.exists(path):
        log.warning("Path to variables file does not exist: %s" % path)
        return dict()

    ini = ConfigParser()
    ini.read(path)

    a = list()
    for section in ini.sections():
        if ":" in section:
            variable_name, _environment = section.split(":")
        else:
            _environment = None
            variable_name = section

        _kwargs = {
            'environment': _environment,
        }
        for key, value in ini.items(section):
            if key == "tags":
                value = split_csv(value)
            else:
                value = smart_cast(value)

            _kwargs[key] = value

        a.append(Variable(variable_name, **_kwargs))

    if environment is not None:
        b = list()
        for var in a:
            if var.environment and var.environment == environment:
                b.append(var)
            elif var.environment is None:
                b.append(var)
            else:
                pass

        return b

    return a

# Classes


class Context(object):

    def __init__(self, **kwargs):
        self.variables = dict()

        for key, value in kwargs.items():
            self.add(key, value)

    def __getattr__(self, item):
        if item in self.variables:
            return self.variables[item].value

        return None

    def __repr__(self):
        return "<%s (%s)>" % (self.__class__.__name__, len(self.variables))

    def add(self, name, value, default=None, environment=None, tags=None):
        v = Variable(name, value, default=default, environment=environment, tags=tags)
        self.variables[name] = v

        return v

    def has(self, name):
        if name not in self.variables:
            return False

        return self.variables[name].value is not None

    def join(self, variables):
        for v in variables:
            self.variables[v.name] = v

    def mapping(self):
        values = dict()
        for key, var in self.variables.items():
            values[key] = var.value or var.default

        return values

    def merge(self, context):
        for name, var in context.variables.items():
            if not self.has(name):
                self.variables[name] = var


class Variable(object):

    def __init__(self, name, value, default=None, environment=None, tags=None):
        self.default = default
        self.environment = environment
        self.name = name
        self.value = value
        self.tags = tags or list()

    def __repr__(self):
        return "<%s %s>" % (self.__class__.__name__, self.name)
