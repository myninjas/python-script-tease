# Imports

from ..commands.apache import MAPPING as APACHE_MAPPING
from ..commands.base import MAPPING as BASE_MAPPING
from ..commands.django import MAPPING as DJANGO_MAPPING
from ..commands.files import MAPPING as FILES_MAPPING
from ..commands.messages import MAPPING as MESSAGES_MAPPING
from ..commands.packages import MAPPING as PACKAGES_MAPPING
from ..commands.pgsql import MAPPING as PGSQL_MAPPING
from ..commands.prompts import MAPPING as PROMPT_MAPPING
from ..commands.services import MAPPING as SERVICES_MAPPING
from ..commands.ssl import MAPPING as SSL_MAPPING
from ..commands.stackscripts import MAPPING as STACKSCRIPTS_MAPPING
from ..commands.templates import MAPPING as TEMPLATES_MAPPING
from ..commands.tarball import MAPPING as TARBALL_MAPPING
from ..commands.users import MAPPING as USERS_MAPPING

# Exports

__all__ = (
    "MAPPING",
    "command_exists",
)

# Constants

MAPPING = dict()
MAPPING.update(APACHE_MAPPING)
MAPPING.update(BASE_MAPPING)
MAPPING.update(DJANGO_MAPPING)
MAPPING.update(FILES_MAPPING)
MAPPING.update(MESSAGES_MAPPING)
MAPPING.update(PACKAGES_MAPPING)
MAPPING.update(PGSQL_MAPPING)
MAPPING.update(PROMPT_MAPPING)
MAPPING.update(SERVICES_MAPPING)
MAPPING.update(SSL_MAPPING)
MAPPING.update(STACKSCRIPTS_MAPPING)
MAPPING.update(TEMPLATES_MAPPING)
MAPPING.update(TARBALL_MAPPING)
MAPPING.update(USERS_MAPPING)

# Functions


def command_exists(name):
    """Determine whether a command name (or alias) exists.

    :param name: The command name or any of its aliases.
    :type name: str

    :rtype: bool

    """
    return name in MAPPING
