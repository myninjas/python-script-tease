# Imports

from myninjas.utils import smart_cast

# Functions


def context_from_cli(variables):
    context = dict()
    for i in variables:
        key, value = i.split(":")
        context[key] = smart_cast(value)

    return context


def filters_from_cli(filters):
    _filters = dict()
    for i in filters:
        key, value = i.split(":")
        if key not in filters:
            _filters[key] = list()

        _filters[key].append(value)

    return _filters


def options_from_cli(options):
    _options = dict()
    for i in options:
        key, value = i.split(":")
        _options[key] = smart_cast(value)

    return _options
