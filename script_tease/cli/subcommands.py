# Imports

from myninjas.shell import EXIT_FAILURE, EXIT_SUCCESS
from ..parsers import load_commands, load_config
from ..utils import highlight_code

# Functions


def output_commands(path, color_enabled=False, context=None, filters=None, locations=None, options=None):
    commands = load_commands(
        path,
        context=context,
        filters=filters,
        locations=locations,
        options=options
    )
    if commands is None:
        return EXIT_FAILURE

    output = list()
    for command in commands:
        statement = command.preview()
        if statement is None:
            continue

        output.append(statement)
        output.append("")

    if color_enabled:
        print(highlight_code("\n".join(output)))
    else:
        print("\n".join(output))

    return EXIT_SUCCESS


def output_docs(path, context=None, filters=None, locations=None, options=None):
    commands = load_commands(
        path,
        context=context,
        filters=filters,
        locations=locations,
        options=options
    )
    if commands is None:
        return EXIT_FAILURE

    count = 1
    output = list()
    for command in commands:
        output.append("%s. %s" % (count, command.comment))
        count += 1

    print("\n".join(output))

    return EXIT_SUCCESS


def output_script(path, color_enabled=False, context=None, filters=None, locations=None, options=None):
    config = load_config(
        path,
        context=context,
        locations=locations,
        options=options
    )
    if config is None:
        return EXIT_FAILURE

    script = config.to_script()
    if color_enabled:
        print(highlight_code(script.to_string()))
    else:
        print(script)

    return EXIT_SUCCESS
