Python Script Tease
===================

Contents:

.. toctree::
    :maxdepth: 2

    Commands <commands>
    Configuration <configuration>
    Developer <developer>
    Tests <tests>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
