adduser
.......

.. code-block:: cfg

    [add a user account]
    adduser = name
    groups = None
    home = None

apache.config
.............

.. code-block:: cfg

    [run an apache config test]
    apache.config = 

apache.configtest
.................

.. code-block:: cfg

    [run an apache config test]
    apache.configtest = 

apache.disable_mod
..................

.. code-block:: cfg

    [disable an apache module]
    apache.disable_mod = module_name

apache.disable_site
...................

.. code-block:: cfg

    [disable a virtual host]
    apache.disable_site = domain_name

apache.enable_mod
.................

.. code-block:: cfg

    [enable an apache module]
    apache.enable_mod = module_name

apache.enable_module
....................

.. code-block:: cfg

    [enable an apache module]
    apache.enable_module = module_name

apache.enable_site
..................

.. code-block:: cfg

    [enable a virtual host]
    apache.enable_site = domain_name

apache.mod
..........

.. code-block:: cfg

    [enable an apache module]
    apache.mod = module_name

apache.module
.............

.. code-block:: cfg

    [enable an apache module]
    apache.module = module_name

apache.test
...........

.. code-block:: cfg

    [run an apache config test]
    apache.test = 

append
......

.. code-block:: cfg

    [append to a file]
    append = path
    content = None

apt
...

.. code-block:: cfg

    [install a package using apt-get]
    apt = package
    remove = False

archive
.......

.. code-block:: cfg

    [create an archive file]
    archive = from_path
    absolute = False
    exclude = None
    file_name = archive.tgz
    strip = 0
    to_path = .
    view = False

certbot
.......

.. code-block:: cfg

    [get new ssl certificate from let's encrypt]
    certbot = domain_name
    email = None
    webroot = None

cmd
...

.. code-block:: cfg

    [a command to be executed]
    cmd = statement
    comment = None
    condition = None
    cwd = None
    environments = None
    function = None
    prefix = None
    register = None
    shell = None
    stop = False
    sudo = None
    tags = None

command
.......

.. code-block:: cfg

    [a command to be executed]
    command = statement
    comment = None
    condition = None
    cwd = None
    environments = None
    function = None
    prefix = None
    register = None
    shell = None
    stop = False
    sudo = None
    tags = None

copy
....

.. code-block:: cfg

    [copy a file or directory]
    copy = from_path to_path
    overwrite = False
    recursive = False

cp
..

.. code-block:: cfg

    [copy a file or directory]
    cp = from_path to_path
    overwrite = False
    recursive = False

django
......

.. code-block:: cfg

    [run a django management command]
    django = name

django.dd
.........

.. code-block:: cfg

    [export django fixtures]
    django.dd = app_name
    file_name = initial
    indent = 4
    natural_foreign = False
    natural_primary = False
    path = None

django.dump
...........

.. code-block:: cfg

    [export django fixtures]
    django.dump = app_name
    file_name = initial
    indent = 4
    natural_foreign = False
    natural_primary = False
    path = None

django.dumpdata
...............

.. code-block:: cfg

    [export django fixtures]
    django.dumpdata = app_name
    file_name = initial
    indent = 4
    natural_foreign = False
    natural_primary = False
    path = None

django.ld
.........

.. code-block:: cfg

    [load django fixtures]
    django.ld = app_name
    file_name = initial
    path = None

django.load
...........

.. code-block:: cfg

    [load django fixtures]
    django.load = app_name
    file_name = initial
    path = None

django.loaddata
...............

.. code-block:: cfg

    [load django fixtures]
    django.loaddata = app_name
    file_name = initial
    path = None

do
..

.. code-block:: cfg

    [a command to be executed]
    do = statement
    comment = None
    condition = None
    cwd = None
    environments = None
    function = None
    prefix = None
    register = None
    shell = None
    stop = False
    sudo = None
    tags = None

echo
....

.. code-block:: cfg

    [run a message command]
    echo = output
    back_title = Message
    dialog = False
    height = 15
    width = 100

extract
.......

.. code-block:: cfg

    [extract an archive]
    extract = from_path
    absolute = False
    exclude = None
    file_name = archive.tgz
    strip = 0
    to_path = None
    view = False

feedback
........

.. code-block:: cfg

    [run a message command]
    feedback = output
    back_title = Message
    dialog = False
    height = 15
    width = 100

include
.......

.. code-block:: cfg

    [include commands from another file]
    include = path
    comment = None
    locations = None

link
....

.. code-block:: cfg

    [create a symlink]
    link = source
    force = False
    target = None

makedir
.......

.. code-block:: cfg

    [create a directory]
    makedir = path
    mode = None
    recursive = True

message
.......

.. code-block:: cfg

    [run a message command]
    message = output
    back_title = Message
    dialog = False
    height = 15
    width = 100

mkdir
.....

.. code-block:: cfg

    [create a directory]
    mkdir = path
    mode = None
    recursive = True

move
....

.. code-block:: cfg

    [move a file or directory]
    move = from_path to_path

msg
...

.. code-block:: cfg

    [run a message command]
    msg = output
    back_title = Message
    dialog = False
    height = 15
    width = 100

mv
..

.. code-block:: cfg

    [move a file or directory]
    mv = from_path to_path

perm
....

.. code-block:: cfg

    [set permissions on a file or directory]
    perm = path
    group = None
    mode = None
    owner = None
    recursive = False

permissions
...........

.. code-block:: cfg

    [set permissions on a file or directory]
    permissions = path
    group = None
    mode = None
    owner = None
    recursive = False

perms
.....

.. code-block:: cfg

    [set permissions on a file or directory]
    perms = path
    group = None
    mode = None
    owner = None
    recursive = False

pg.client
.........

.. code-block:: cfg

    [execute a psql command]
    pg.client = sql
    database = template1
    host = localhost
    password = None
    port = 5432
    user = postgres

pg.createdatabase
.................

.. code-block:: cfg

    [create a postgresql database]
    pg.createdatabase = name
    admin_pass = None
    admin_user = postgres
    host = localhost
    owner = None
    port = 5432
    template = None

pg.createdb
...........

.. code-block:: cfg

    [create a postgresql database]
    pg.createdb = name
    admin_pass = None
    admin_user = postgres
    host = localhost
    owner = None
    port = 5432
    template = None

pg.createuser
.............

.. code-block:: cfg

    [create a postgresql user]
    pg.createuser = name
    admin_pass = None
    admin_user = postgres
    host = localhost
    password = None
    port = 5432

pg.database
...........

.. code-block:: cfg

    [create a postgresql database]
    pg.database = name
    admin_pass = None
    admin_user = postgres
    host = localhost
    owner = None
    port = 5432
    template = None

pg.db
.....

.. code-block:: cfg

    [create a postgresql database]
    pg.db = name
    admin_pass = None
    admin_user = postgres
    host = localhost
    owner = None
    port = 5432
    template = None

pg.dropdatabase
...............

.. code-block:: cfg

    [remove a postgresql database]
    pg.dropdatabase = name
    admin_pass = None
    admin_user = postgres
    host = localhost
    port = 5432

pg.dropdb
.........

.. code-block:: cfg

    [remove a postgresql database]
    pg.dropdb = name
    admin_pass = None
    admin_user = postgres
    host = localhost
    port = 5432

pg.dropuser
...........

.. code-block:: cfg

    [remove a postgres user]
    pg.dropuser = name
    admin_pass = None
    admin_user = postgres
    host = localhost
    port = 5432

pg.dump
.......

.. code-block:: cfg

    [export a postgres database]
    pg.dump = name
    admin_pass = None
    admin_user = postgres
    file_name = None
    host = localhost
    port = 5432

pg.dumpdb
.........

.. code-block:: cfg

    [export a postgres database]
    pg.dumpdb = name
    admin_pass = None
    admin_user = postgres
    file_name = None
    host = localhost
    port = 5432

pg.exists
.........

.. code-block:: cfg

    [determine if a postgres database exists]
    pg.exists = name
    admin_pass = None
    admin_user = postgres
    host = localhost
    port = 5432

pg.user
.......

.. code-block:: cfg

    [create a postgresql user]
    pg.user = name
    admin_pass = None
    admin_user = postgres
    host = localhost
    password = None
    port = 5432

pip
...

.. code-block:: cfg

    [install a python package using pip]
    pip = package
    remove = False
    upgrade = False

prompt
......

.. code-block:: cfg

    [prompt the user for input]
    prompt = name
    back_title = Prompt
    choices = None
    default = None
    dialog = False
    help_text = None
    label = None

psql
....

.. code-block:: cfg

    [execute a psql command]
    psql = sql
    database = template1
    host = localhost
    password = None
    port = 5432
    user = postgres

reload
......

.. code-block:: cfg

    [reload a service]
    reload = service

remove
......

.. code-block:: cfg

    [remove a file or directory]
    remove = path
    force = False
    recursive = False

rename
......

.. code-block:: cfg

    [move a file or directory]
    rename = from_path to_path

replace
.......

.. code-block:: cfg

    [replace text in a file]
    replace = path
    backup = .b
    change = None
    delimiter = /
    find = None

restart
.......

.. code-block:: cfg

    [restart a service]
    restart = service

rm
..

.. code-block:: cfg

    [remove a file or directory]
    rm = path
    force = False
    recursive = False

rsync
.....

.. code-block:: cfg

    [synchronize files from a local to remote directory]
    rsync = source target
    delete = False
    guess = False
    host = None
    key_file = None
    links = True
    port = 22
    recursive = True
    user = None

run
...

.. code-block:: cfg

    [a command to be executed]
    run = statement
    comment = None
    condition = None
    cwd = None
    environments = None
    function = None
    prefix = None
    register = None
    shell = None
    stop = False
    sudo = None
    tags = None

scp
...

.. code-block:: cfg

    [copy a file from the local (machine) to the remote host]
    scp = from_path to_path
    host = None
    key_file = None
    port = 22
    user = None

sed
...

.. code-block:: cfg

    [replace text in a file]
    sed = path
    backup = .b
    change = None
    delimiter = /
    find = None

slack
.....


This uses the Incoming Webhooks feature, which requires some additional setup.

.. note::
    The following steps were accurate as of April 2019.

**1.** Log in to Slack and go to `Your Apps`_.

.. _Your Apps: https://api.slack.com/apps

**2.** Create a new Slack app.

**3.** On the next page, select Incoming Webhooks and then toggle activation.

.. image:: /_static/images/slack-1.jpg

**4.** Next click Add new Webhook to Workspace and select the channel to which the message will be posted.

.. image:: /_static/images/slack-2.jpg

.. image:: /_static/images/slack-3.jpg

**5.** Copy the URL for the new webhook to use as the ``url`` parameter for the Slack command.
    


.. code-block:: cfg

    [send a message to slack]
    slack = message
    url = None

ssl
...

.. code-block:: cfg

    [get new ssl certificate from let's encrypt]
    ssl = domain_name
    email = None
    webroot = None

stackscript.udf
...............

.. code-block:: cfg

    [create a udf input for a linode stackscript]
    stackscript.udf = name
    default = None
    example = None
    label = None

start
.....

.. code-block:: cfg

    [start a service]
    start = service

stop
....

.. code-block:: cfg

    [stop a service]
    stop = service

symlink
.......

.. code-block:: cfg

    [create a symlink]
    symlink = source
    force = False
    target = None

sync
....

.. code-block:: cfg

    [synchronize files from a local to remote directory]
    sync = source target
    delete = False
    guess = False
    host = None
    key_file = None
    links = True
    port = 22
    recursive = True
    user = None

template
........

.. code-block:: cfg

    [parse a template]
    template = source target
    backup = True
    parser = jina2

touch
.....

.. code-block:: cfg

    [touch a file or directory]
    touch = path

tpl
...

.. code-block:: cfg

    [parse a template]
    tpl = source target
    backup = True
    parser = jina2

udf
...

.. code-block:: cfg

    [create a udf input for a linode stackscript]
    udf = name
    default = None
    example = None
    label = None

venv
....

.. code-block:: cfg

    [create a python virtual environment]
    venv = name

virtualenv
..........

.. code-block:: cfg

    [create a python virtual environment]
    virtualenv = name

write
.....

.. code-block:: cfg

    [write to a file]
    write = path
    content = None
    overwrite = False

yum
...

.. code-block:: cfg

    [install a package using yum]
    yum = package
    remove = False

