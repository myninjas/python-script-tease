.. _configuration:

=============
Configuration
=============

Generating Commands From a File
-------------------------------

The :py:class:`script_tease.parsers.config.Config` class may instantiate commands by loading a configuration file.

.. note::
    Additional formats such as JSON or YAML may be supported in the future.

An example file:

.. code-block:: ini

    [install apache]
    apt = apache2

    [create the website directory]
    mkdir = /var/www/example_com/www
    recursive = yes

    [set permissions on the website directory]
    perms = /var/www/example_com/www
    group = www-data
    mode = 775
    owner = www-data


Notes regarding this format:

- This is the standard format for Python's ConfigParser.
- The first line is the INI section and is used as the default comment.
- The command name must be the *first* option in the section.
- The arguments for the command appear as the value of the first option in the section. Arguments are separated by a
  space.
- Arguments that should be treated as a single value should be enclosed in double quotes.
- ``yes`` and ``no`` are interpreted as boolean values.
- List values, where required, are separated by commas.

Pre-Parsing Command Files as Templates
--------------------------------------

Configuration file may be pre-processed as a Jinja2 template by providing a context dictionary:

.. code-block:: ini

    [install apache]
    apt = apache

    [create the website directory]
    mkdir = /var/www/{{ domain_tld }}/www
    recursive = yes

    [set permissions on the website directory]
    perms = /var/www/{{ domain_tld }}/www
    group= www-data
    mode = 775
    owner www-data

Then with a config instance:

.. code-block:: python

    context = {
        'domain_tld': "example_com",
    }

    config = Config("commands.ini", context=context)
    config.load()

    for command in config.get_commands():
        print(command.preview())
        print("")


Using the Tease Command
-----------------------

The ``tease`` command may be used to parse a configuration file, providing additional utilities for working with
commands.

.. code-block:: text

    positional arguments:
      path                  The path to the configuration file.

    optional arguments:
      -h, --help            show this help message and exit
      -c, --color           Enable code highlighting for terminal output.
      -C= VARIABLES, --context= VARIABLES
                            Context variables for use in pre-parsing the config
                            and templates. In the form of: name:value
      -d, --docs            Output documentation instead of code.
      -D, --debug           Enable debug output.
      -f= FILTERS, --filter= FILTERS
                            Filter the commands in the form of: attribute:value
      -O= OPTIONS, --option= OPTIONS
                            Common command options in the form of: name:value
      -T= TEMPLATE_LOCATIONS, --template-path= TEMPLATE_LOCATIONS
                            The location of template files that may be used with
                            the template command.
      -w= OUTPUT_FILE, --write= OUTPUT_FILE
                            Write the output to disk.
      -v                    Show version number and exit.
      --version             Show verbose version information and exit.

The ``path`` argument defaults to ``commands.ini``.
