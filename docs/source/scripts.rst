.. _scripts:

=======
Scripts
=======

Commands may be instantiated and added to a script instance.

.. code-block:: python

    from script_tease.commands.files import MakeDir, Touch
    from script_tease.scripts import Script

    script = Script("testing")
    script.append(MakeDir("/var/www/html"))
    script.append(Touch("/var/www/html/index.html"))
    print(script)

