.. _commands:

========
Commands
========

Script Tease ships with a library of classes that help pre-define a number of command line activities. These may be used
programmatically, assembled using :py:class:`script_tease.scripts.Script`, or automated using a configuration file.

1. For programmatic use, see the :ref:`developer-reference`.
2. TODO: Add scripts documentation.
3. See :ref:`configuration` for automated commands generation with a configuration file.

Common Parameters
-----------------

All commands support the following common parameters:

- ``comment``: A comment regarding the command.
- ``condition``: A condition for execution. For example, ``! -f /path/to/some/file.txt``
- ``cwd``: The path from which a command should be executed.
- ``environments``: A string or list of strings indicating the operational environments in which the command runs. This
  is *not* used by default, but may be used to programmatically filter commands for a specific environment. For example,
  development versus live.
- ``prefix``: A statement to be added prior to executing the command.
- ``register``: A variable name to which the the success or failure (exit code) of the statement is captured.
- ``shell``: The shell used to run the commands. For example, ``/bin/bash``. This is generally not important, but can
  be a problem when attempting to execute some commands (such as Django management commands).
- ``stop``: ``True`` indicates no other commands should be executed if the given command fails.
- ``sudo``: ``True`` indicates the command should be automatically prefixed with ``sudo``. If provided as a string, the
  command is also prefixed with a specific user name.
- ``tags``: A list of tags used to classify the command.

Defining an "Itemized" Command
------------------------------

Certain command definitions may be repeated by defining a list of items.

Example of an "itemized" command:

.. code-block:: ini

    [touch a bunch of files]
    touch = /var/www/example_com/www/$item
    items = index.html, assets/index.html, content/index.html

.. note::
    Command itemization may vary with the command type. Use preview to test your results.

Available Commands
------------------

The following commands instantiate command classes.

Commands with examples:

.. include:: _command-examples.rst